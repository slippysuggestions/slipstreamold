import discord
import random
import json
import aiohttp
import time
import asyncio
import re
import inspect
from discord.ext import commands, tasks
from discord import utils
from random import sample
from datetime import datetime


bot = commands.Bot(command_prefix='!',
                   description='no')


def biglist(thing):
    return {
            '[]':'Not active', '{"sfn":505}':'Ruse War Field', '{"sfn":510}':'Gian Point', '{"sfn":550}':'Nsu Grid', '{"sfn":551}':'Ganalen`sGrave', '{"sfn":552}':'Rya', '{"sfn":553}':'Flexa', '{"sfn":554}':'H-2 Cloud', '{"sfn":555}':'R-9 Cloud',
            #missiontypes 
            'MT_ARTIFACT':'Disruption', 'MT_ASSAULT':'Assault', 'MT_ASSASSINATION':'Assassination', 'MT_CAPTURE':'Capture', 'MT_DEFENSE':'Defense', 'MT_DISRUPTION':'Disruption', 'MT_EVACUATION':'Defection', 'MT_EXCAVATE':'Excavation', 'MT_EXTERMINATION':'Extermination', 'MT_HIVE':'HiveSabotage', 'MT_INTEL':'Spy', 'MT_LANDSCAPE':'PlainsofEidolon', 'MT_MOBILE_DEFENSE':'MobileDefense', 'MT_RESCUE':'Rescue', 'MT_RETRIEVAL':'Hijack', 'MT_SABOTAGE':'Sabotage', 'MT_SECTOR':'DarkSector', 'MT_SURVIVAL':'Survival', 'MT_TERRITORY':'Interception',
            #relictiers
            'VoidT1':'Lith', 'VoidT2':'Meso', 'VoidT3':'Neo', 'VoidT4':'Axi', 'VoidT5':'Requiem',
            #relays
            'PlutoHUB':'OrcusRelay(Pluto)', 'SaturnHUB':'KroniaRelay(Saturn)',
            #factions-Invasions
            'FC_INFESTATION':'Infestation', 'FC_CORPUS':'Corpus', 'FC_GRINEER':'Grineer',
            #items-Invasions
            '/Lotus/Types/Items/Research/EnergyComponent':'Fieldron', '/Lotus/Types/Items/Research/ChemComponent':'DetoniteAmpulle','/Lotus/Types/Recipes/Components/FormaBlueprint':'Forma Blueprint','/Lotus/Types/Recipes/Weapons/WeaponParts/StrunWraithReceiver':'Strun Wraith Receiver',
            #modifiers
            "SORTIE_MODIFIER_LOW_ENERGY":"EnergyReduction", "SORTIE_MODIFIER_IMPACT":"EnemyPhysicalEnhancement:Impact", "SORTIE_MODIFIER_SLASH":"EnemyPhysicalEnhancement:Slash", "SORTIE_MODIFIER_PUNCTURE":"EnemyPhysicalEnhancement:Puncture", "SORTIE_MODIFIER_EXIMUS":"EximusStronghold", "SORTIE_MODIFIER_MAGNETIC":"EnemyElementalEnhancement:Magnetic", "SORTIE_MODIFIER_CORROSIVE":"EnemyElementalEnhancement:Corrosive", "SORTIE_MODIFIER_VIRAL":"EnemyElementalEnhancement:Viral", "SORTIE_MODIFIER_ELECTRICITY":"EnemyElementalEnhancement:Electricity", "SORTIE_MODIFIER_RADIATION":"EnemyElementalEnhancement:Radiation", "SORTIE_MODIFIER_GAS":"EnemyElementalEnhancement:Gas", "SORTIE_MODIFIER_FIRE":"EnemyElementalEnhancement:Heat", "SORTIE_MODIFIER_EXPLOSION":"EnemyElementalEnhancement:Blast", "SORTIE_MODIFIER_FREEZE":"EnemyElementalEnhancement:Cold", "SORTIE_MODIFIER_TOXIN":"EnemyElementalEnhancement:Toxin", "SORTIE_MODIFIER_POISON":"EnemyElementalEnhancement:Toxin", "SORTIE_MODIFIER_HAZARD_RADIATION":"EnvironmentalHazard:RadiationPockets", "SORTIE_MODIFIER_HAZARD_MAGNETIC":"EnvironmentalHazard:ElectromagneticAnomalies", "SORTIE_MODIFIER_HAZARD_FOG":"EnvironmentalHazard:DenseFog", "SORTIE_MODIFIER_HAZARD_FIRE":"EnvironmentalHazard:Fire", "SORTIE_MODIFIER_HAZARD_ICE":"EnvironmentalEffect:CryogenicLeakage", "SORTIE_MODIFIER_HAZARD_COLD":"EnvironmentalEffect:ExtremeCold", "SORTIE_MODIFIER_ARMOR":"AugmentedEnemyArmor", "SORTIE_MODIFIER_SHIELDS":"EnhancedEnemyShields", "SORTIE_MODIFIER_SECONDARY_ONLY":"WeaponRestriction:PistolOnly", "SORTIE_MODIFIER_SHOTGUN_ONLY":"WeaponRestriction:ShotgunOnly", "SORTIE_MODIFIER_SNIPER_ONLY":"WeaponRestriction:SniperOnly", "SORTIE_MODIFIER_RIFLE_ONLY":"WeaponRestriction:AssaultRifleOnly", "SORTIE_MODIFIER_MELEE_ONLY":"WeaponRestriction:MeleeOnly", "SORTIE_MODIFIER_BOW_ONLY":"WeaponRestriction:BowOnly",
            #items
            "/Lotus/StoreItems/Upgrades/Mods/Rifle/BowMultiShotOnHitMod":"SplitFlights", "/Lotus/StoreItems/Upgrades/Skins/Hoverboard/HoverboardStickerBaroA":"Blue Ki'Teer Safari K-Drive Scrawl",
            #mods
            "/Lotus/StoreItems/Upgrades/Mods/Pistol/DualStat/FireEventPistolMod":"Scorch","/Lotus/StoreItems/Upgrades/Mods/Melee/DualStat/FireEventMeleeMod":"Volcanic Edge","/Lotus/StoreItems/Upgrades/Mods/Rifle/WeaponEventSlashDamageMod":"Fanged Fusilade","/Lotus/StoreItems/Weapons/Tenno/Melee/MeleeTrees/ClawCmbTwoMeleeTree":"Vermillion Storm",
            #baro items
            "None":"None","/Lotus/StoreItems/Upgrades/Skins/VoidTrader/BaroInarosPolearmSkin":"Anpu Staff Skin","/Lotus/StoreItems/Upgrades/Skins/SummerSolstice/SummerSolsticeTwinGrakatas":"Twin Grakata Towsun Skin","/Lotus/StoreItems/Weapons/Grineer/LongGuns/GrineerLeverActionRifle/PrismaGrinlokWeapon":"Prisma Grinlok","/Lotus/StoreItems/Weapons/Grineer/Melee/GrineerMachetteAndCleaver/PrismaDualCleavers":"Prisma Dual Cleavers"
            }.get(thing, 'Not Yet Defined')

Solnodes = {'SolNode0': {'value': 'SolNode0', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'SolNode1': {'value': 'Galatea (Neptune)', 'enemy': 'Corpus', 'type': 'Capture'}, 'SolNode2': {'value': 'Aphrodite (Venus)', 'enemy': 'Corpus', 'type': 'MobileDefense'}, 'SolNode3': {'value': 'Cordelia (Uranus)', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'SolNode4': {'value': 'Acheron (Pluto)', 'enemy': 'Corpus', 'type': 'Exterminate'}, 
'SolNode5': {'value': 'Perdita (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode6': {'value': 'Despina (Neptune)', 'enemy': 'Corpus', 'type': 'Excavation'}, 'SolNode7': {'value': 'Epimetheus (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode8': {'value': 'Nix (Pluto)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode9': {'value': 'Rosalind (Uranus)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode10': {'value': 'Thebe (Jupiter)', 'enemy': 'Corpus', 'type': 'Sabotage'}, 'SolNode11': {'value': 'Tharsis (Mars)', 'enemy': 'Corpus', 'type': 'Hijack'}, 'SolNode12': {'value': 'Elion (Mercury)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode13': {'value': 'Bianca (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode14': {'value': 'Ultor (Mars)', 'enemy': 'Crossfire', 'type': 'Exterminate'}, 'SolNode15': {'value': 'Pacific (Earth)', 'enemy': 'Grineer', 'type': 'Rescue'}, 'SolNode16': {'value': 'Augustus (Mars)', 'enemy': 'Grineer', 'type': 'Excavation'}, 'SolNode17': {'value': 'Proteus (Neptune)', 'enemy': 'Corpus', 'type': 'Defense'}, 'SolNode18': {'value': 'Rhea (Saturn)', 'enemy': 'Grineer', 'type': 'Interception'}, 'SolNode19': {'value': 'Enceladus (Saturn)', 'enemy': 'Grineer', 'type': 'Sabotage'}, 'SolNode20': {'value': 'Telesto (Saturn)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode21': {'value': 'Narcissus (Pluto)', 'enemy': 'Corpus', 'type': 'Exterminate'}, 'SolNode22': {'value': 'Tessera (Venus)', 'enemy': 'Corpus', 'type': 
'Defense'}, 'SolNode23': {'value': 'Cytherean (Venus)', 'enemy': 'Corpus', 'type': 'Interception'}, 'SolNode24': {'value': 'Oro (Earth)', 'enemy': 'Grineer', 'type': 'Assassination'}, 'SolNode25': {'value': 'Callisto (Jupiter)', 'enemy': 'Corpus', 'type': 'Interception'}, 'SolNode26': {'value': 'Lith (Earth)', 'enemy': 'Grineer', 'type': 'Defense'}, 'SolNode27': {'value': 'EPrime (Earth)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 
'SolNode28': {'value': 'MPrime (Mercury)', 'enemy': 'Crossfire', 'type': 'Exterminate'}, 'SolNode29': {'value': 'Oberon (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode30': {'value': 'Olympus (Mars)', 'enemy': 'Grineer', 'type': 'Disruption'}, 'SolNode31': {'value': 'Anthe (Saturn)', 'enemy': 'Grineer', 'type': 'Rescue'}, 'SolNode32': {'value': 'Tethys (Saturn)', 'enemy': 'Grineer', 'type': 'Assassination'}, 'SolNode33': {'value': 'Ariel (Uranus)', 'enemy': 'Grineer', 'type': 'Sabotage'}, 'SolNode34': {'value': 'Sycorax (Uranus)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode35': {'value': 'Arcadia (Mars)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode36': {'value': 'Martialis (Mars)', 'enemy': 'Grineer', 'type': 'Rescue'}, 'SolNode37': {'value': 'Pallene (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode38': {'value': 'Minthe (Pluto)', 'enemy': 'Corpus', 'type': 'MobileDefense'}, 'SolNode39': {'value': 'Everest (Earth)', 'enemy': 'Grineer', 'type': 'Excavation'}, 'SolNode40': {'value': 'Prospero (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode41': {'value': 'Arval (Mars)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode42': {'value': 'Helene (Saturn)', 'enemy': 'Grineer', 'type': 'Defense'}, 'SolNode43': {'value': 'Cerberus (Pluto)', 'enemy': 'Corpus', 'type': 'Interception'}, 'SolNode44': {'value': 'Mimas (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode45': {'value': 'Ara (Mars)', 'enemy': 'Grineer', 
'type': 'Capture'}, 'SolNode46': {'value': 'Spear (Mars)', 'enemy': 'Grineer', 'type': 'Defense'}, 'SolNode47': {'value': 'Janus (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode48': {'value': 'Regna (Pluto)', 'enemy': 'Corpus', 'type': 'Rescue'}, 'SolNode49': {'value': 'Larissa (Neptune)', 'enemy': 'Corpus', 'type': 'MobileDefense'}, 'SolNode50': {'value': 'Numa (Saturn)', 'enemy': 'Grineer', 'type': 'Rescue'}, 
'SolNode51': {'value': 'Hades (Pluto)', 'enemy': 'Corpus', 'type': 'Assassination'}, 'SolNode52': {'value': 'Portia (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode53': {'value': 'Themisto (Jupiter)', 'enemy': 'Corpus', 'type': 'Assassination'}, 'SolNode54': {'value': 'Silvanus (Mars)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode55': {'value': 'Methone (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode56': {'value': 'Cypress (Pluto)', 'enemy': 'Corpus', 'type': 'Sabotage'}, 'SolNode57': {'value': 'Sao (Neptune)', 'enemy': 'Corpus', 'type': 'Sabotage'}, 'SolNode58': {'value': 'Hellas (Mars)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode59': {'value': 'Eurasia (Earth)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode60': {'value': 'Caliban (Uranus)', 'enemy': 'Grineer', 'type': 'Rescue'}, 'SolNode61': {'value': 'Ishtar (Venus)', 'enemy': 'Corpus', 'type': 'Sabotage'}, 'SolNode62': {'value': 'Neso (Neptune)', 'enemy': 'Corpus', 'type': 'Exterminate'}, 'SolNode63': {'value': 'Mantle (Earth)', 'enemy': 'Grineer', 'type': 'Capture'}, 'SolNode64': {'value': 'Umbriel (Uranus)', 'enemy': 'Grineer', 'type': 'Interception'}, 'SolNode65': {'value': 'Gradivus (Mars)', 'enemy': 'Corpus', 'type': 'Sabotage'}, 'SolNode66': {'value': 'Unda (Venus)', 'enemy': 'Corpus', 'type': 'Spy'}, 'SolNode67': {'value': 'Dione (Saturn)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode68': {'value': 'Vallis (Mars)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode69': {'value': 'Ophelia (Uranus)', 'enemy': 'Grineer', 'type': 'Survival'}, 'SolNode70': {'value': 'Cassini (Saturn)', 'enemy': 'Grineer', 'type': 'Capture'}, 'SolNode71': {'value': 'Vesper (Venus)', 'enemy': 'Corpus', 'type': 'Spy'}, 'SolNode72': {'value': 'OuterTerminus (Pluto)', 'enemy': 'Corpus', 'type': 'Defense'}, 'SolNode73': {'value': 'Ananke (Jupiter)', 'enemy': 'Corpus', 'type': 'Capture'}, 'SolNode74': {'value': 'Carme (Jupiter)', 'enemy': 'Corpus', 'type': 'MobileDefense'}, 'SolNode75': {'value': 'Cervantes (Earth)', 'enemy': 'Grineer', 'type': 'Sabotage'}, 'SolNode76': {'value': 'Hydra (Pluto)', 'enemy': 'Corpus', 'type': 'Capture'}, 'SolNode77': {'value': 'Cupid (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode78': {'value': 'Triton (Neptune)', 'enemy': 'Corpus', 'type': 'Rescue'}, 'SolNode79': {'value': 'Cambria (Earth)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode80': {'value': 'Phoebe (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode81': {'value': 'Palus (Pluto)', 'enemy': 'Corpus', 'type': 'Survival'}, 'SolNode82': {'value': 'Calypso (Saturn)', 'enemy': 'Grineer', 'type': 'Sabotage'}, 'SolNode83': {'value': 'Cressida (Uranus)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode84': {'value': 'Nereid (Neptune)', 'enemy': 'Corpus', 'type': 'Hijack'}, 'SolNode85': {'value': 'Gaia (Earth)', 'enemy': 'Grineer', 'type': 'Interception'}, 'SolNode86': {'value': 'Aegaeon (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode87': {'value': 'Ganymede (Jupiter)', 'enemy': 'Corpus', 'type': 'Disruption'}, 'SolNode88': {'value': 'Adrastea (Jupiter)', 'enemy': 'Corpus', 'type': 'Spy'}, 'SolNode89': {'value': 'Mariana (Earth)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode90': {'value': 'Miranda', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'SolNode91': {'value': 'Iapetus (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode92': {'value': 'Charon (Pluto)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode93': {'value': 'Keeler (Saturn)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode94': {'value': 'Apollodorus (Mercury)', 'enemy': 'Infested', 'type': 'Survival'}, 'SolNode95': {'value': 'Thalassa (Neptune)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode96': {'value': 'Titan (Saturn)', 'enemy': 'Grineer', 'type': 'Survival'}, 'SolNode97': {'value': 'Amalthea (Jupiter)', 'enemy': 'Corpus', 'type': 'Spy'}, 'SolNode98': {'value': 'Desdemona (Uranus)', 'enemy': 'Grineer', 'type': 'Capture'}, 'SolNode99': {'value': 'War (Mars)', 'enemy': 'Grineer', 'type': 'Assassinate'}, 'SolNode100': {'value': 'Elara (Jupiter)', 'enemy': 'Corpus', 'type': 'Survival'}, 'SolNode101': {'value': 'Kiliken (Venus)', 'enemy': 'Corpus', 'type': 'Excavation'}, 'SolNode102': {'value': 'Oceanum (Pluto)', 'enemy': 'Corpus', 'type': 'Spy'}, 'SolNode103': {'value': 'Terminus (Mercury)', 'enemy': 'Grineer', 'type': 'Capture'}, 'SolNode104': {'value': 'Fossa (Venus)', 'enemy': 'Corpus', 'type': 'Assassinate'}, 'SolNode105': {'value': 'Titania (Uranus)', 'enemy': 'Grineer', 'type': 'Assassination'}, 'SolNode106': {'value': 'Alator (Mars)', 'enemy': 'Grineer', 'type': 'Interception'}, 'SolNode107': {'value': 'Venera (Venus)', 'enemy': 'Corpus', 'type': 'Capture'}, 'SolNode108': {'value': 'Tolstoj (Mercury)', 'enemy': 'Grineer', 'type': 'Assassinate'}, 'SolNode109': {'value': 'Linea (Venus)', 'enemy': 'Corpus', 'type': 'Rescue'}, 'SolNode110': {'value': 'Hyperion (Saturn)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode111': {'value': 'Juliet (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode112': {'value': 'Setebos (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode113': {'value': 'Ares (Mars)', 'enemy': 'Grineer', 'type': 'Sabotage'}, 'SolNode114': {'value': 'Puck (Uranus)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode115': {'value': 'Quirinus (Mars)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode116': {'value': 'Mab (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode117': {'value': 'Naiad (Neptune)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode118': {'value': 'Laomedeia (Neptune)', 'enemy': 'Corpus', 'type': 'Disruption'}, 'SolNode119': {'value': 'Caloris (Mercury)', 'enemy': 'Grineer', 'type': 
'Rescue'}, 'SolNode120': {'value': 'Halimede (Neptune)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode121': {'value': 'Carpo (Jupiter)', 'enemy': 'Corpus', 'type': 'Exterminate'}, 'SolNode122': {'value': 'Stephano (Uranus)', 'enemy': 'Grineer', 'type': 'Defense'}, 'SolNode123': {'value': 'VPrime (Venus)', 'enemy': 'Corpus', 'type': 'Survival'}, 'SolNode124': {'value': 'Trinculo (Uranus)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode125': {'value': 'Io (Jupiter)', 'enemy': 'Corpus', 'type': 'Defense'}, 'SolNode126': {'value': 'Metis (Jupiter)', 'enemy': 'Corpus', 'type': 'Rescue'}, 'SolNode127': {'value': 'Psamathe (Neptune)', 'enemy': 'Corpus', 'type': 'Assassination'}, 'SolNode128': {'value': 'EGate (Venus)', 'enemy': 'Corpus', 'type': 'Exterminate'}, 'SolNode129': {'value': 'OrbVallis (Venus)', 'enemy': 'Corpus', 'type': 'FreeRoam'}, 'SolNode130': {'value': 'Lares (Mercury)', 'enemy': 'Grineer', 'type': 'Defense'}, 'SolNode131': {'value': 'Pallas (Ceres)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode132': {'value': 'Bode (Ceres)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode133': {'value': 'Vedic (Ceres)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode134': {'value': 'Varro (Ceres)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode135': {'value': 'Thon (Ceres)', 'enemy': 'Grineer', 'type': 'Sabotage'}, 'SolNode136': {'value': 'Olla (Ceres)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode137': {'value': 'Nuovo (Ceres)', 'enemy': 'Grineer', 'type': 'Rescue'}, 'SolNode138': {'value': 'Ludi (Ceres)', 'enemy': 'Grineer', 'type': 'Hijack'}, 'SolNode139': {'value': 'Lex (Ceres)', 'enemy': 'Grineer', 'type': 'Capture'}, 'SolNode140': {'value': 'Kiste (Ceres)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode141': {'value': 'Ker (Ceres)', 'enemy': 'Grineer', 'type': 'Sabotage'}, 'SolNode142': {'value': 'Hapke (Ceres)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode143': {'value': 'Gefion (Ceres)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode144': {'value': 'Exta (Ceres)', 'enemy': 'Grineer', 'type': 'Assassination'}, 'SolNode145': {'value': 'Egeria (Ceres)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode146': {'value': 'Draco (Ceres)', 'enemy': 'Grineer', 'type': 'Survival'}, 'SolNode147': {'value': 'Cinxia (Ceres)', 'enemy': 'Grineer', 'type': 'Interception'}, 'SolNode148': {'value': 'Cerium (Ceres)'}, 'SolNode149': {'value': 'Casta (Ceres)', 'enemy': 'Grineer', 'type': 'Defense'}, 'SolNode150': {'value': 'Albedo (Ceres)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode151': {'value': 'Acanth (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode152': {'value': 'Ascar (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode153': {'value': 
'Brugia (Eris)', 'enemy': 'Infested', 'type': 'Rescue'}, 'SolNode154': {'value': 'Candiru (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode155': {'value': 'Cosis (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode156': {'value': 'Cyath (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode157': {'value': 'Giardia (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode158': {'value': 'Gnathos (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode159': {'value': 'Lepis (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode160': {'value': 'Histo (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode161': {'value': 'Hymeno (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode162': {'value': 'Isos (Eris)', 'enemy': 'Infested', 'type': 'Capture'}, 'SolNode163': {'value': 'Ixodes (Eris)'}, 'SolNode164': {'value': 'Kala-azar (Eris)', 'enemy': 'Infested', 'type': 'Defense'}, 'SolNode165': {'value': 'Sporid (Eris)', 'enemy': 'Infested', 'type': 'HiveSabotage'}, 'SolNode166': {'value': 'Nimus (Eris)', 'enemy': 'Infested', 'type': 'Survival'}, 'SolNode167': {'value': 'Oestrus (Eris)', 'enemy': 'Infested', 'type': 'MobileDefense'}, 'SolNode168': {'value': 'Phalan (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode169': {'value': 'Psoro (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode170': {'value': 'Ranova (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode171': {'value': 'Saxis (Eris)', 'enemy': 'Infested', 'type': 'Exterminate'}, 'SolNode172': {'value': 'Xini (Eris)', 'enemy': 'Infested', 'type': 'Interception'}, 'SolNode173': {'value': 'Solium (Eris)', 'enemy': 'Infested', 'type': 'MobileDefense'}, 'SolNode174': {'value': 'Sparga (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode175': {'value': 'Naeglar (Eris)', 'enemy': 'Infested', 'type': 'Hive'}, 'SolNode176': {'value': 'Viver (Eris)', 'enemy': 'Infested', 'type': 'AncientRetribution'}, 'SolNode177': {'value': 'Kappa (Sedna)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode178': {'value': 'Hyosube (Sedna)', 
'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode179': {'value': 'Jengu (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode180': {'value': 'Undine (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode181': {'value': 'Adaro (Sedna)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode182': {'value': 'Camenae (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode183': {'value': 'Vodyanoi (Sedna)', 'enemy': 'Grineer', 'type': 'Arena'}, 'SolNode184': {'value': 'Rusalka (Sedna)', 'enemy': 'Grineer', 'type': 'Capture'}, 'SolNode185': {'value': 'Berehynia (Sedna)', 'enemy': 'Grineer', 'type': 'Interception'}, 'SolNode186': {'value': 'Phithale (Sedna)', 'enemy': 'Grineer', 'type': 'Sabotage'}, 'SolNode187': {'value': 'Selkie (Sedna)', 'enemy': 'Grineer', 'type': 'Survival'}, 'SolNode188': {'value': 'Kelpie (Sedna)', 'enemy': 'Grineer', 'type': 'Disruption'}, 'SolNode189': {'value': 'Naga (Sedna)', 'enemy': 'Grineer', 'type': 'Rescue'}, 'SolNode190': {'value': 'Nakki (Sedna)', 'enemy': 'Grineer', 'type': 'Arena'}, 'SolNode191': {'value': 'Marid (Sedna)', 'enemy': 'Grineer', 'type': 'Hijack'}, 'SolNode192': {'value': 'Tikoloshe (Sedna)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode193': {'value': 'Merrow (Sedna)', 'enemy': 'Grineer', 'type': 'Assassination'}, 'SolNode194': {'value': 'Ponaturi (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode195': {'value': 'Hydron (Sedna)', 'enemy': 'Grineer', 'type': 'Defense'}, 'SolNode196': {'value': 'Charybdis (Sedna)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode197': {'value': 'Graeae (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode198': {'value': 'Scylla (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode199': {'value': 'Yam (Sedna)', 'enemy': 'Grineer', 'type': 'Arena'}, 'SolNode200': {'value': 'Veles (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode201': {'value': 'Tiamat (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode202': {'value': 'Yemaja (Sedna)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode203': {'value': 'Abaddon (Europa)', 'enemy': 'Corpus', 'type': 'Capture'}, 'SolNode204': {'value': 'Armaros (Europa)', 'enemy': 'Corpus', 'type': 'Exterminate'}, 'SolNode205': {'value': 'Baal (Europa)', 'enemy': 'Corpus', 'type': 'Exterminate'}, 'SolNode206': {'value': 'Eligor (Europa)'}, 'SolNode207': {'value': 'Gamygyn (Europa)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode208': {'value': 'Lillith (Europa)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode209': {'value': 'Morax (Europa)', 'enemy': 'Corpus', 'type': 'MobileDefense'}, 'SolNode210': {'value': 'Naamah (Europa)', 'enemy': 'Corpus', 'type': 'Assassination'}, 'SolNode211': {'value': 'Ose (Europa)', 'enemy': 'Corpus', 'type': 'Interception'}, 'SolNode212': {'value': 'Paimon (Europa)', 'enemy': 'Corpus', 'type': 'Defense'}, 'SolNode213': {'value': 'Shax (Europa)'}, 'SolNode214': {'value': 'Sorath (Europa)', 'enemy': 
'Corpus', 'type': 'Hijack'}, 'SolNode215': {'value': 'Valac (Europa)', 'enemy': 'Corpus', 'type': 'Spy'}, 'SolNode216': {'value': 'Valefor (Europa)', 'enemy': 'Corpus', 'type': 'Excavation'}, 'SolNode217': {'value': 'Orias (Europa)', 'enemy': 'Corpus', 'type': 'Rescue'}, 'SolNode218': {'value': 'Zagan (Europa)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode219': {'value': 'Beleth (Europa)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SolNode220': {'value': 'Kokabiel (Europa)', 'enemy': 'Corpus', 'type': 'Sabotage'}, 'SolNode221': {'value': 'Neruda (Mercury)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode222': {'value': 'Eminescu (Mercury)', 'enemy': 'Grineer', 'type': 'AncientRetribution'}, 'SolNode223': {'value': 'Boethius (Mercury)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode224': {'value': 'Odin (Mercury)', 'enemy': 'Grineer', 'type': 'Interception'}, 'SolNode225': {'value': 'Suisei (Mercury)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode226': {'value': 'Pantheon (Mercury)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode227': {'value': 'Verdi (Mercury)'}, 'SolNode228': {'value': 'PlainsofEidolon (Earth)', 'enemy': 'Grineer', 'type': 'FreeRoam'}, 'SolNode400': {'value': 'Teshub (Void)', 'enemy': 'Orokin', 'type': 'Exterminate'}, 'SolNode401': {'value': 'Hepit (Void)', 'enemy': 'Orokin', 'type': 'Capture'}, 'SolNode402': {'value': 'Taranis (Void)', 'enemy': 'Orokin', 'type': 'Defense'}, 'SolNode403': {'value': 'Tiwaz (Void)', 'enemy': 'Orokin', 'type': 'MobileDefense'}, 'SolNode404': {'value': 'Stribog (Void)', 'enemy': 'Orokin', 'type': 'OrokinSabotage'}, 'SolNode405': {'value': 'Ani (Void)', 'enemy': 'Orokin', 'type': 'Survival'}, 'SolNode406': {'value': 'Ukko (Void)', 'enemy': 'Orokin', 'type': 'Capture'}, 'SolNode407': {'value': 'Oxomoco (Void)', 'enemy': 'Orokin', 'type': 'Exterminate'}, 'SolNode408': {'value': 'Belenus (Void)', 'enemy': 'Orokin', 'type': 'Defense'}, 'SolNode409': {'value': 'Mot (Void)', 'enemy': 'Orokin', 'type': 'Survival'}, 'SolNode410': {'value': 'Aten (Void)', 'enemy': 'Orokin', 'type': 'MobileDefense'}, 'SolNode411': {'value': 'SolNode411 (Void)', 'enemy': 'Orokin', 'type': 'AncientRetribution'}, 'SolNode412': {'value': 'Mithra (Void)', 'enemy': 'Orokin', 'type': 'Interception'}, 'SolNode413': {'value': 'SolNode413 (Void)', 'enemy': 'Corrupted', 'type': 'AncientRetribution'}, 'SolNode740': {'value': 'TheRopalolyst (Jupiter)', 'enemy': 'Sentient', 'type': 'Assassination'}, 'SolNode741': {'value': 'Koro (Kuva Fortress)', 'enemy': 'Grineer', 'type': 'Assault'}, 'SolNode742': {'value': 'Nabuk (Kuva Fortress)', 'enemy': 'Grineer', 'type': 'Capture'}, 'SolNode743': {'value': 'Rotuma (Kuva Fortress)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode744': {'value': 'Taveuni (Kuva Fortress)', 'enemy': 'Grineer', 'type': 'Survival'}, 'SolNode745': {'value': 'Tamu (Kuva Fortress)', 'enemy': 'Grineer', 'type': 'Disruption'}, 'SolNode746': {'value': 'Dakata (Kuva Fortress)', 'enemy': 'Grineer', 'type': 'Extermination'}, 'SolNode747': {'value': 'Pago (Kuva Fortress)', 'enemy': 'Grineer', 'type': 'Spy'}, 'SolNode748': {'value': 'Garus (Kuva Fortress)', 'enemy': 'Grineer', 'type': 'Rescue'}, 'SolNode901': {'value': 'Caduceus', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'SolNode902': {'value': 'Montes (Venus)', 'enemy': 'Corpus', 'type': 'Exterminate (Archwing)'}, 'SolNode903': {'value': 'Erpo (Earth)', 'enemy': 'Grineer', 'type': 'MobileDefense (Archwing)'}, 'SolNode904': {'value': 'Syrtis (Mars)', 'enemy': 'Grineer', 'type': 'Exterminate (Archwing)'}, 'SolNode905': {'value': 'Galilea (Jupiter)', 'enemy': 'Corpus', 'type': 'Sabotage (Archwing)'}, 'SolNode906': {'value': 'Pandora (Saturn)', 'enemy': 'Grineer', 'type': 'Pursuit (Archwing)'}, 'SolNode907': {'value': 'Caelus (Uranus)', 'enemy': 'Grineer', 'type': 'Interception (Archwing)'}, 'SolNode908': {'value': 'Salacia (Neptune)', 'enemy': 'Corpus', 'type': 'MobileDefense (Archwing)'}, 'SolNode300': {'value': 'Plato (Lua)', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'SolNode301': {'value': 'Grimaldi (Lua)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode302': {'value': 'Tycho (Lua)', 'enemy': 'Corpus', 'type': 'Survival'}, 'SolNode304': {'value': 'Copernicus (Lua)', 'enemy': 'Grineer', 'type': 'MobileDefense'}, 'SolNode305': {'value': 'Stöfler (Lua)', 'enemy': 'Corpus', 'type': 'Survival'}, 'SolNode306': {'value': 'Pavlov (Lua)', 'enemy': 'Corpus', 'type': 'Spy'}, 'SolNode307': {'value': 'Zeipel (Lua)', 'enemy': 'Corpus', 'type': 'Rescue'}, 'SolNode308': {'value': 'Apollo (Lua)', 'enemy': 'Corpus', 'type': 'Disruption'}, 'SettlementNode1': {'value': 'Roche (Phobos)', 'enemy': 'Corpus', 'type': 'Exterminate'}, 'SettlementNode2': {'value': 'Skyresh (Phobos)', 'enemy': 'Corpus', 'type': 'Capture'}, 'SettlementNode3': {'value': 'Stickney (Phobos)', 'enemy': 'Corpus', 'type': 'Survival'}, 'SettlementNode4': {'value': 'Drunlo (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode5': {'value': 'Grildrig (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode6': {'value': 'Limtoc (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode7': {'value': 'Hall (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode8': {'value': 'Reldresal (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode9': {'value': 'Clustril (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode10': {'value': 'Kepler (Phobos)', 'enemy': 'Corpus', 'type': 'Rush (Archwing)'}, 'SettlementNode11': {'value': 'Gulliver (Phobos)', 'enemy': 'Corpus', 'type': 'Defense'}, 'SettlementNode12': {'value': 'Monolith (Phobos)', 'enemy': 'Corpus', 'type': 'Rescue'}, 'SettlementNode13': {'value': "D'Arrest (Phobos)", 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode14': {'value': 'Shklovsky (Phobos)', 'enemy': 'Corpus', 'type': 'Spy'}, 'SettlementNode15': {'value': 'Sharpless (Phobos)', 'enemy': 'Corpus', 'type': 'MobileDefense'}, 'SettlementNode16': {'value': 'Wendell (Phobos)', 'enemy': 'Corpus', 
'type': 'AncientRetribution'}, 'SettlementNode17': {'value': 'Flimnap (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode18': {'value': 'Opik (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode19': {'value': 'Todd (Phobos)', 'enemy': 'Corpus', 'type': 'AncientRetribution'}, 'SettlementNode20': {'value': 'Iliad (Phobos)', 'enemy': 'Corpus', 'type': 'Assassination'}, 'MercuryHUB': {'value': 'LarundaRelay (Mercury)', 'enemy': 'Grineer', 'type': 'Relay'}, 'VenusHUB': {'value': 'VesperRelay (Venus)', 'enemy': 'Corpus', 'type': 'Relay'}, 'EarthHUB': {'value': 'StrataRelay (Earth)', 'enemy': 'Grineer', 'type': 'Relay'}, 'SaturnHUB': {'value': 'KroniaRelay (Saturn)', 'enemy': 'Grineer', 'type': 'Relay'}, 'ErisHUB': {'value': 'KuiperRelay (Eris)', 'enemy': 'Infested', 'type': 'Relay'}, 'EuropaHUB': {'value': 'LeonovRelay (Europa)', 'enemy': 'Corpus', 'type': 'Relay'}, 'PlutoHUB': {'value': 'OrcusRelay (Pluto)', 'enemy': 'Corpus', 'type': 'Relay'}, 'EventNode0': {'value': 'Balor', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode1': {'value': 'Tethra', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode2': {'value': 'OperationGateCrash', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode3': {'value': 'Elatha', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode4': {'value': 'ProxyRebellion', 'enemy': 'Corpus', 'type': 'Survival'}, 'EventNode5': {'value': 'Birog', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode6': {'value': 'TylReygorSealLab', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode7': {'value': 'ProxyRebellion', 'enemy': 'Corpus', 'type': 'Interception'}, 'EventNode8': {'value': 'Corb', 
'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode9': {'value': 'OperationGateCrashPt.2', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode10': {'value': 'Lugh', 'enemy': 'Sentient', 'type': 
'AncientRetribution'}, 'EventNode11': {'value': 'Nemed', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode12': {'value': 'OperationCryoticFront', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode13': {'value': 'ShiftingSands', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode14': {'value': 'GateCrash', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode15': {'value': 'OperationCryoticFront', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode16': {'value': 'OperationCryoticFront', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode17': {'value': 'ProxyRebellion', 'enemy': 'Corpus', 'type': 'Defense'}, 'EventNode18': {'value': 'ProxyRebellion', 'enemy': 'Corpus', 'type': 'Defense'}, 'EventNode19': {'value': 'Mars', 'enemy': 'Grineer', 'type': 'Defense'}, 'EventNode20': {'value': 'TylRegorSeaLab', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode22': {'value': 'TylRegorSeaLab', 'enemy': 'Sentient', 'type': 'AncientRetribution'}, 'EventNode24': {'value': 'Earth', 'enemy': 'Grineer', 
'type': 'Arena'}, 'EventNode25': {'value': 'Earth', 'enemy': 'Grineer', 'type': 'Arena'}, 'EventNode26': {'value': 'Earth', 'enemy': 'Grineer', 'type': 'Exterminate'}, 'EventNode27': {'value': 'Void', 'enemy': 'Corrupted', 'type': 'Survival'}, 'EventNode28': {'value': 'Saturn', 'enemy': 'Grineer', 'type': 'Assassination'}, 'EventNode29': {'value': 'Saturn', 'enemy': 'Grineer', 'type': 'Assassination'}, 'EventNode30': {'value': 'Ganymede (Jupiter)', 'enemy': 'Corpus', 'type': 'Disruption'}, 'EventNode31': {'value': 'Ganymede (Jupiter)', 'enemy': 'Corpus', 'type': 'Disruption'}, 'EventNode32': {'value': 'Ganymede (Jupiter)', 'enemy': 'Corpus', 'type': 'Disruption'}, 'EventNode33': {'value': 'Ganymede (Jupiter)', 'enemy': 'Corpus', 'type': 'Disruption'}, 'EventNode34': {'value': 'Earth', 'enemy': 'Grineer', 'type': 'Arena'}, 'EventNode35': {'value': 'Earth', 
'enemy': 'Grineer', 'type': 'Arena'}, 'EventNode761': {'value': 'TheIndex', 'enemy': 'Corpus', 'type': 'Arena'}, 'EventNode762': {'value': 'TheIndexpt2', 'enemy': 'Corpus', 'type': 'Arena'}, 'EventNode763': {'value': 'TheIndexEndurance', 'enemy': 'Corpus', 'type': 'Arena'}, 'PvpNode0': {'value': 'ConclaveCapturetheCephalon', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode1': {'value': 'Conclave', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode2': {'value': 'Conclave', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode3': {'value': 'ConclaveCapturetheCephalon', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode4': {'value': 'Conclave', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode5': {'value': 'Conclave', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode6': {'value': 'Conclave', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode7': {'value': 'Conclave', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode8': {'value': 'Conclave', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode9': {'value': 'ConclaveTeamDomination', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode10': {'value': 'ConclaveDomination', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode11': {'value': 'ConclaveDomination', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode12': {'value': 'ConclaveDomination', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode13': {'value': 'TacticalAlert:SnoballFight!', 'enemy': 'Tenno', 'type': 'Conclave'}, 'PvpNode14': {'value': 'Conclave:QuickSteel', 'enemy': 'Tenno', 'type': 'Conclave'}, 'ClanNode0': {'value': 'Romula (Venus)', 'enemy': 'Infested', 'type': 'DarkSectorDefense'}, 'ClanNode1': {'value': 'Malva (Venus)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode2': {'value': 'Coba (Earth)', 'enemy': 'Infested', 'type': 'DarkSectorDefense'}, 'ClanNode3': {'value': 'Tikal (Earth)', 'enemy': 'Infested', 'type': 'DarkSectorExcavation'}, 'ClanNode4': {'value': 'Sinai (Jupiter)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode5': {'value': 'Cameria (Jupiter)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode6': {'value': 'Larzac (Europa)', 'enemy': 'Infested', 'type': 'DarkSectorDefense'}, 'ClanNode7': {'value': 'Cholistan (Europa)', 'enemy': 'Infested', 'type': 'DarkSectorExcavation'}, 'ClanNode8': {'value': 'Kadesh (Mars)', 'enemy': 'Infested', 'type': 'DarkSectorDefense'}, 'ClanNode9': {'value': 'Wahiba (Mars)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode10': {'value': 'Memphis (Phobos)', 'enemy': 'Infested', 'type': 'DarkSectorDefection'}, 'ClanNode11': {'value': 'Zeugma (Phobos)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode12': {'value': 'Caracol (Saturn)', 'enemy': 'Infested', 'type': 'DarkSectorDefection'}, 'ClanNode13': {'value': 'Piscinas (Saturn)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode14': {'value': 'Amarna (Sedna)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode15': {'value': 'Sangeru (Sedna)', 'enemy': 'Infested', 'type': 'DarkSectorDefense'}, 'ClanNode16': {'value': 'Ur (Uranus)', 'enemy': 'Infested', 'type': 'DarkSectorDisruption'}, 'ClanNode17': {'value': 'Assur (Uranus)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode18': {'value': 'Akkad (Eris)', 'enemy': 'Infested', 'type': 'DarkSectorDefense'}, 'ClanNode19': {'value': 'Zabala (Eris)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode20': {'value': 'Yursa (Neptune)', 'enemy': 'Infested', 'type': 'DarkSectorDefection'}, 'ClanNode21': {'value': 'Kelashin (Neptune)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode22': {'value': 'Seimeni (Ceres)', 'enemy': 'Infested', 'type': 'DarkSectorDefense'}, 'ClanNode23': {'value': 'Gabii (Ceres)', 'enemy': 'Infested', 'type': 'DarkSectorSurvival'}, 'ClanNode24': {'value': 'Sechura (Pluto)', 'enemy': 'Infested', 'type': 'DarkSectorDefense'}, 'ClanNode25': {'value': 'Hieracon (Pluto)', 'enemy': 'Infested', 'type': 'DarkSectorExcavation'}, '/Lotus/Types/Keys/SortieBossKeyPhorid': {'value': 'SortieBoss:Phorid', 'enemy': 'Infested', 'type': 'Assassination'}, 'CrewBattleNode505': {'value': 'RuseWarField (Veil Proxima)', 'enemy': 'Grineer', 'type': 'Skirmish'}, 'CrewBattleNode510': {'value': 'GianPoint (Veil Proxima)', 'enemy': 'Grineer', 'type': 'Skirmish'}, 'CrewBattleNode550': {'value': 'NsuGrid (Veil Proxima)', 'enemy': 'Grineer', 'type': 'Skirmish'}, 'CrewBattleNode551': {'value': "Ganalen'sGrave (Veil Proxima)", 'enemy': 'Grineer', 'type': 'Skirmish'}, 'CrewBattleNode552': {'value': 'Rya (Veil Proxima)', 'enemy': 'Grineer', 'type': 'Skirmish'}, 'CrewBattleNode553': {'value': 'Flexa (Veil Proxima)', 'enemy': 'Grineer', 'type': 'Skirmish'}, 'CrewBattleNode554': {'value': 'H-2Cloud (Veil Proxima)', 'enemy': 'Grineer', 'type': 'Skirmish'}, 'CrewBattleNode555': {'value': 'R-9Cloud (Veil Proxima)', 'enemy': 'Grineer', 'type': 'Skirmish'}}
k = json.dumps(Solnodes)
k = json.loads(k)

class embedColor:
    orange = 0xec5c17
    yellow = 0xFFFF00
    blurple = 0x7289DA
    blue = 0x499FFF 
    darkblue = 0x000072
    grey = 0xCCCCCC 
    red = 0xc90b0b
    green = 0x00ff0f


class maincommands(commands.Cog):
    def __init__(self, bot):
        self.bot:commands.Bot = bot
        self._last_result = None
        self.http = aiohttp.ClientSession()


    @commands.command()
    async def anomaly(self, ctx):
        async with self.http.get('http://content.warframe.com/dynamic/worldState.php') as r:
            j = await r.json(content_type = None)
        anomalypos = j['Tmp']
        embed = discord.Embed(title='Sentient Anomaly Location',
                              color=0x52fffc,
                              description=biglist(anomalypos))
        await ctx.send(embed=embed)


    @commands.command()
    async def fissures(self, ctx):
        async with self.http.get('http://content.warframe.com/dynamic/worldState.php') as r:
            j = await r.json(content_type = None)

        num = j['ActiveMissions']
        num = len(num)
        lithnodes = []
        mesonodes = []
        neonodes = []
        axinodes = []
        reqnodes = []

        embed = discord.Embed(title='Active Fissures',
                                color=0x52fffc)

        for number in range(num):
            fisnodetype = j['ActiveMissions'][number]['MissionType']
            fisnodeexp = j['ActiveMissions'][number]['Expiry']['$date']['$numberLong']
            fisnodeexp = datetime.fromtimestamp(int(fisnodeexp)/1000) - datetime.now()
            fisnoderaw = j['ActiveMissions'][number]['Node']
            fisnodemod = j['ActiveMissions'][number]['Modifier']
            fissurenode = k[fisnoderaw]['value']

            if fisnodemod == 'VoidT1':
                lithnodes.append(biglist(fisnodetype) + ' - ' + fissurenode + ' expires in: ' + str(fisnodeexp)[:7] + "\n")

            if fisnodemod == 'VoidT2':
                mesonodes.append(biglist(fisnodetype) + ' - ' + fissurenode + ' expires in: ' + str(fisnodeexp)[:7] + "\n")

            if fisnodemod == 'VoidT2':
                neonodes.append(biglist(fisnodetype) + ' - ' + fissurenode + ' expires in: ' + str(fisnodeexp)[:7] + "\n")

            if fisnodemod == 'VoidT3':
                axinodes.append(biglist(fisnodetype) + ' - ' + fissurenode + ' expires in: ' + str(fisnodeexp)[:7] + "\n")

            if fisnodemod == 'VoidT4':
                reqnodes.append(biglist(fisnodetype) + ' - ' + fissurenode + ' expires in: ' + str(fisnodeexp)[:7] + "\n")

        embed.add_field(name= '**Lith**', value= str(''.join(lithnodes)), inline=False)
        embed.add_field(name= '**Meso**', value= str(''.join(mesonodes)), inline=False)
        embed.add_field(name= '**Neo**', value= str(''.join(neonodes)), inline=False)
        embed.add_field(name= '**Axi**', value= str(''.join(axinodes)), inline=False)
        embed.add_field(name= '**Requiem**', value= str(''.join(reqnodes)), inline=False)
        await ctx.send(embed=embed)
                

    @commands.command()
    async def arbitration(self, ctx):
        async with self.http.get('https://10o.io/kuvalog.json') as r:
            j = await r.json(content_type = None)

        arbyloc = j[0]['solnodedata']['tile']
        arbyplanet = j[0]['solnodedata']['planet']
        arbymissiontype = j[0]['solnodedata']['type']
        arbyfaction = j[0]['solnodedata']['enemy']

        embed = discord.Embed(title='Arby Location', description=f'Node: {arbyloc} ({arbyplanet})\nMissiontype: {arbymissiontype}\nFaction: {arbyfaction}',color=0x52fffc)
        await ctx.send(embed=embed)


    @commands.command()
    async def baro(self,ctx):
        async with self.http.get('http://content.warframe.com/dynamic/worldState.php') as r:
            j = await r.json(content_type = None)

        lengthBaroList = len(j['VoidTraders'][0])

        if  lengthBaroList <= 5:
            baroplace = j['VoidTraders'][0]['Node']
            barotime = j['VoidTraders'][0]['Activation']['$date']['$numberLong']
            barotime = datetime.fromtimestamp(int(barotime)/1000) - datetime.now()
            barotime = str(barotime).split('.')[0]
            embed = discord.Embed(title="Void Trader", description="Baro Ki'Teer will appear in: " + barotime + " on the " + biglist(baroplace), color=0x53dff7)
            await ctx.send(embed=embed)

        else:
            length = len(j['VoidTraders'][0]['Manifest'])
            baroitems = [[]]
            pagenum = 0
            entrycounter = 0
            page = 0
            baroLeaveTime = j['VoidTraders'][0]['Expiry']['$date']['$numberLong']
            baroLeaveTime = datetime.fromtimestamp(int(baroLeaveTime)/1000) - datetime.now()
            baroLeaveTime = str(baroLeaveTime).split('.')[0]

            for i in range(length):
                item = j['VoidTraders'][0]['Manifest'][i]['ItemType']
                item_ducat_price = j['VoidTraders'][0]['Manifest'][i]['PrimePrice']
                item_credits_price = j['VoidTraders'][0]['Manifest'][i]['RegularPrice']
                baroitems[pagenum].append('**' + biglist(item) + '**\n' + str(item_ducat_price) +'<:ducats:565728007264796703>\n' + str(item_credits_price) + '<:credits:565728007315128330>\n')
                entrycounter += 1
                if entrycounter == 5:
                    baroitems.append([])
                    pagenum += 1
                    entrycounter = 0

            embed = discord.Embed(title="Void Trader - Page " + str(page + 1), description=''.join(baroitems[page]), color=0x53dff7)
            embed.set_footer(text='Void Trader is leaving in ' + baroLeaveTime)

            msg = await ctx.send(embed=embed)
            await msg.add_reaction('⬅️')
            await msg.add_reaction('➡️')

            uses = 0
            def check(reaction, user):
                return reaction.message.id == msg.id and user == ctx.author

            while uses < 15:
                try:
                    reaction, _ = await self.bot.wait_for('reaction_add', timeout=20.0, check=check)
                    if reaction.emoji == '⬅️' and page > 0:
                        page -= 1
                        uses += 1
                        embed = discord.Embed(title="Void Trader - Page " + str(page + 1), description=''.join(baroitems[page]), color=0x53dff7)
                        embed.set_footer(text='Void Trader is leaving in ' + baroLeaveTime)
                        await msg.edit(embed=embed)
                    if reaction.emoji == '➡️' and page < len(baroitems)-1:
                        page += 1
                        uses += 1
                        embed = discord.Embed(title="Void Trader - Page " + str(page + 1), description=''.join(baroitems[page]), color=0x53dff7)
                        embed.set_footer(text='Void Trader is leaving in ' + baroLeaveTime)
                        await msg.edit(embed=embed)

                except asyncio.TimeoutError:
                    uses = 15


    @commands.command()
    async def remind(self, ctx, time:str, *, message):
        timenum = re.findall("[0-9]+", time)
        try:
            timenum = int("".join(timenum))
        except ValueError:
            timenum = 0
        timeunit = re.findall("[smhd]", time)
        timeunit = "".join(timeunit)

        def timeconvert(unit):
            return {
                's' :1,
                'm' :60,
                'h' :3600,
                'd' :86400,
                }.get(unit, 0)

        if timeconvert(timeunit) == 0 or timenum == 0:
            await ctx.send('Invalid Arguments: `s/remind <time> <message>`')
        else:
            waittime = timenum*timeconvert(timeunit)
            await asyncio.sleep(waittime)
            await ctx.send(f'{ctx.message.author.mention} You asked me to remind you of: `{message}`, {timenum}{timeunit} ago')


    @commands.command()
    async def invasions(self, ctx):
        async with self.http.get('http://content.warframe.com/dynamic/worldState.php') as r:
            j = await r.json(content_type = None)

        invaslist = [[]]
        invasnodes = [[]]
        pagenum = 0
        entrycounter = 0

        for number in range(len(j['Invasions'])):
            invnode = j['Invasions'][number]['Node']
            invnode = k[invnode]['value']
            invatkfac = j['Invasions'][number]['Faction']
            invdeffac = j['Invasions'][number]['DefenderFaction']

            if invatkfac == 'FC_INFESTATION':
                invatkrew = 'None'
                invdefrew = j['Invasions'][number]['DefenderReward']['countedItems'][0]['ItemType']
            else:
                invatkrew = j['Invasions'][number]['AttackerReward']['countedItems'][0]['ItemType']
                invdefrew = j['Invasions'][number]['DefenderReward']['countedItems'][0]['ItemType']

            invasnodes[pagenum].append(f'**{invnode}**')
            invaslist[pagenum].append(f'**Attackers:** {biglist(invatkfac)} \n**Defenders:** {biglist(invdeffac)} \n**A.Rewards:** {biglist(invatkrew)} \n**D.Rewards:** {biglist(invdefrew)}')
            entrycounter += 1

            if entrycounter == 9:
                invaslist.append([])
                invasnodes.append([])
                pagenum += 1
                entrycounter = 0

        embed = discord.Embed(title='Active Invasions - Page 1', color=0x53dff7)

        for i in range(len(invaslist[0])):
            embed.add_field(name=invasnodes[0][i], value=invaslist[0][i])

        msg = await ctx.send(embed=embed)
        await msg.add_reaction('⬅️')
        await msg.add_reaction('➡️')

        page = 0
        uses = 0

        def check(reaction, user):
            return reaction.message.id == msg.id and user == ctx.author

        while uses < 15:
            try:
                reaction, _ = await self.bot.wait_for('reaction_add', timeout=20.0, check=check)
                if reaction.emoji == '⬅️' and page > 0:
                    page -= 1
                    uses += 1
                    embed = discord.Embed(title=f'Active Invasions - Page {page+1}', color=0x53dff7)
                    for i in range(len(invaslist[page])):
                        embed.add_field(name=invasnodes[page][i], value=invaslist[page][i])
                    await msg.edit(embed=embed)

                if reaction.emoji == '➡️' and page < len(invasnodes)-1:
                    page += 1
                    uses += 1
                    embed = discord.Embed(title=f'Active Invasions - Page {page+1}', color=0x53dff7)

                    for i in range(len(invaslist[page])):
                        embed.add_field(name=invasnodes[page][i], value=invaslist[page][i])
                    await msg.edit(embed=embed)

                await msg.remove_reaction(reaction.emoji, ctx.author)
            except asyncio.TimeoutError:
                uses = 15

    @commands.command()
    async def sortie(self, ctx):
        async with self.http.get('http://content.warframe.com/dynamic/worldState.php') as r:
            j = await r.json(content_type=None)
        node1type = j["Sorties"][0]["Variants"][0]["missionType"]
        node1mod = j["Sorties"][0]["Variants"][0]["modifierType"]
        node1mis = j["Sorties"][0]["Variants"][0]["node"]

        node2type = j["Sorties"][0]["Variants"][1]["missionType"]
        node2mod = j["Sorties"][0]["Variants"][1]["modifierType"]
        node2mis = j["Sorties"][0]["Variants"][1]["node"]

        node3type = j["Sorties"][0]["Variants"][2]["missionType"]
        node3mod = j["Sorties"][0]["Variants"][2]["modifierType"]
        node3mis = j["Sorties"][0]["Variants"][2]["node"]

        embed = discord.Embed(title="<:Sorties:654967335232208896> Daily Sortie",
                              description=f"**1.** {biglist(node1type)} - {k[node1mis]['value']}\n**Modifier:** {biglist(node1mod)}\n**2.** {biglist(node2type)} - {k[node2mis]['value']}\n**Modifier:** {biglist(node2mod)}\n**3.** {biglist(node3type)} - {k[node3mis]['value']}\n**Modifier:** {biglist(node3mod)}",
                              color=0x52fffc)

        embed.set_footer(text="Sortie resets = Today at 11:00 AM")
        await ctx.send(embed=embed)

    @commands.command()
    async def nightwave(self, ctx):
        async with self.http.get('http://content.warframe.com/dynamic/worldState.php') as r:
            j = await r.json(content_type=None)

        async with self.http.get('http://slipstream-bot.com/slipstream/nightwaveChallenges.json') as c:
            b = await c.json(content_type=None)

        weeklies = []
        dailies = []
        eliteWeeklies = []

        try:
            expiry = j['SeasonInfo']['ActiveChallenges'][len(j['SeasonInfo']['ActiveChallenges'])-1]['Expiry']['$date']['$numberLong']
            expiry = datetime.fromtimestamp(int(expiry)/1000) - datetime.now()
            expiry = str(expiry).split('.')[0]

            embed = discord.Embed(title='Nightwave Challenges', color=0x52fffc)
            for i in range(len(j['SeasonInfo']['ActiveChallenges'])):
                challenge = j['SeasonInfo']['ActiveChallenges'][i]['Challenge']
                try:
                    clgtrslt = b['NightwaveChallenges'][challenge]
                except KeyError:
                    clgtrslt = challenge.replace('/Lotus/Types/Challenges/Seasons/', '')
                if '/Weekly/' in challenge:
                    weeklies.append(f"**{i + 1}.** {clgtrslt}\n")

                elif '/Daily/' in challenge:
                    dailies.append(f"**{i + 1}.** {clgtrslt}\n")

                elif '/WeeklyHard/' in challenge:
                    eliteWeeklies.append(f"**{i + 1}.** {clgtrslt}\n")

            embed.add_field(name='Daily Challenges', value=''.join(dailies), inline=False)
            embed.add_field(name='Weekly Challenges', value=''.join(weeklies), inline=False)
            embed.add_field(name='Elite Weekly Challenges', value=''.join(eliteWeeklies), inline=False)
            embed.set_footer(text='Store and Weekly Challenges reset in: ' + expiry)

            await ctx.send(embed=embed)

        except KeyError:
            embed = discord.Embed(description='Nightwave is not active currently.', color=embedColor.blurple)
            await ctx.send(embed=embed)

    @commands.command()
    async def weapon(self, ctx, *, name):
        async with self.http.get('http://slipstream-bot.com/slipstream/weapons.json') as r:
            j = await r.json(content_type=None)

        name = name.title()

        if name not in j:
            await ctx.send('Weapon not found! Try again.')
            return

        MeleeWithPassive = j[name]['weaponSlot'] == 'Melee' and j[name]['passive'] != 'None'
        MeleeWithoutPassive = j[name]['weaponSlot'] == 'Melee' and j[name]['passive'] == 'None'

        PrimaryWithRangeAndPassive = j[name]['weaponSlot'] == 'Primary' and j[name]['range'] != 'None' and j[name]['passive'] != 'None'
        PrimaryWithRangeOnly = j[name]['weaponSlot'] == 'Primary' and j[name]['range'] != 'None' and j[name]['passive'] == 'None'
        PrimaryWithPassiveOnly = j[name]['weaponSlot'] == 'Primary' and j[name]['range'] == 'None' and j[name]['passive'] != 'None'
        PrimaryWithoutRangeAndPassive = j[name]['weaponSlot'] == 'Primary' and j[name]['range'] == 'None' and j[name]['passive'] == 'None'

        SecondaryWithRangeAndPassive = j[name]['weaponSlot'] == 'Secondary' and j[name]['range'] != 'None' and j[name]['passive'] != 'None'
        SecondaryWithRangeOnly = j[name]['weaponSlot'] == 'Secondary' and j[name]['range'] != 'None' and j[name]['passive'] == 'None'
        SecondaryWithPassiveOnly = j[name]['weaponSlot'] == 'Secondary' and j[name]['range'] == 'None' and j[name]['passive'] != 'None'
        SecondaryWithoutRangeAndPassive = j[name]['weaponSlot'] == 'Secondary' and j[name]['range'] == 'None' and j[name]['passive'] == 'None'

        embed = discord.Embed(title=name,
                              description=f"**Mastery Rank:** {j[name]['masteryReq']} \n"
                                          f"**Obtained:** {j[name]['obtained']}\n"
                                          f"**Slot:** {j[name]['weaponSlot']}",
                              color=0x53dff)
        embed.add_field(name='**Core Stats**',
                        value=f"**Total Damage:** {j[name]['totalDamage']}\n"
                              f"{j[name]['damagewTypes']}\n"
                              f"**Critical Chance:** {j[name]['critchance']}\n"
                              f"**Critical Multiplier:** {j[name]['critMulti']}\n"
                              f"**Status Chance:** {j[name]['statusChance']}\n"
                              f"**Riven Dispo:** {j[name]['rivenDispo']}")

        if MeleeWithPassive:
            embed.add_field(name='**Utility**',
                            value=f"**Attack Speed:** {j[name]['attackSpeed']}\n"
                                  f"**Range:** {j[name]['range']}\n"
                                  f"**Combo Duration**: {j[name]['comboDur']}\n"
                                  f"**Disposition:** {j[name]['rivenDispo']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Stance Polarity:** {j[name]['stancePol']}\n"
                                  f"**Passive:** {j[name]['passive']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif MeleeWithoutPassive:
            embed.add_field(name='**Utility**',
                            value=f"**Attack Speed:** {j[name]['attackSpeed']}\n"
                                  f"**Range:** {j[name]['range']}\n"
                                  f"**Combo Duration**: {j[name]['comboDur']}\n"
                                  f"**Disposition:** {j[name]['rivenDispo']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Stance Polarity:** {j[name]['stancePol']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif PrimaryWithRangeAndPassive:
            embed.add_field(name='**Utility**',
                            value=f"**Fire rate:** {j[name]['fireRate']}\n"
                                  f"**Range:** {j[name]['range']}\n"
                                  f"**Mag Size:** {j[name]['magazineSize']}\n"
                                  f"**Reload Speed:** {j[name]['reloadTime']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Trigger Type:** {j[name]['triggerType']}\n"
                                  f"**Passive:** {j[name]['passive']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif PrimaryWithRangeOnly:
            embed.add_field(name='**Utility**',
                            value=f"**Fire rate:** {j[name]['fireRate']}\n"
                                  f"**Range:** {j[name]['range']}\n"
                                  f"**Mag Size:** {j[name]['magazineSize']}\n"
                                  f"**Reload Speed:** {j[name]['reloadTime']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Trigger Type:** {j[name]['triggerType']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif PrimaryWithPassiveOnly:
            embed.add_field(name='**Utility**',
                            value=f"**Fire rate:** {j[name]['fireRate']}\n"
                                  f"**Mag Size:** {j[name]['magazineSize']}\n"
                                  f"**Reload Speed:** {j[name]['reloadTime']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Passive:** {j[name]['passive']}\n"
                                  f"**Trigger Type:** {j[name]['triggerType']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif PrimaryWithoutRangeAndPassive:
            embed.add_field(name='**Utility**',
                            value=f"**Fire rate:** {j[name]['fireRate']}\n"
                                  f"**Mag Size:** {j[name]['magazineSize']}\n"
                                  f"**Reload Speed:** {j[name]['reloadTime']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Trigger Type:** {j[name]['triggerType']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif SecondaryWithRangeAndPassive:
            embed.add_field(name='**Utility**',
                            value=f"**Fire rate:** {j[name]['fireRate']}\n"
                                  f"**Range:** {j[name]['range']}\n"
                                  f"**Mag Size:** {j[name]['magazineSize']}\n"
                                  f"**Reload Speed:** {j[name]['reloadTime']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Trigger Type:** {j[name]['triggerType']}\n"
                                  f"**Passive:** {j[name]['passive']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif SecondaryWithRangeOnly:
            embed.add_field(name='**Utility**',
                            value=f"**Fire rate:** {j[name]['fireRate']}\n"
                                  f"**Range:** {j[name]['range']}\n"
                                  f"**Mag Size:** {j[name]['magazineSize']}\n"
                                  f"**Reload Speed:** {j[name]['reloadTime']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Trigger Type:** {j[name]['triggerType']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif SecondaryWithPassiveOnly:
            embed.add_field(name='**Utility**',
                            value=f"**Fire rate:** {j[name]['fireRate']}\n"
                                  f"**Mag Size:** {j[name]['magazineSize']}\n"
                                  f"**Reload Speed:** {j[name]['reloadTime']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Passive:** {j[name]['passive']}\n"
                                  f"**Trigger Type:** {j[name]['triggerType']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        elif SecondaryWithoutRangeAndPassive:
            embed.add_field(name='**Utility**',
                            value=f"**Fire rate:** {j[name]['fireRate']}\n"
                                  f"**Mag Size:** {j[name]['magazineSize']}\n"
                                  f"**Reload Speed:** {j[name]['reloadTime']}\n"
                                  f"**Type:** {j[name]['weaponType']}\n"
                                  f"**Trigger Type:** {j[name]['triggerType']}\n"
                                  f"**Innate Polarities:** {j[name]['inPol']}\n"
                                  f"**Exilus Polarity:** {j[name]['exPol']}")

        else:
            embed = discord.Embed(description='Oops, something went wrong!', color=0x53dff)

        embed.set_footer(text=f'{ctx.author.name}#{ctx.author.discriminator}', icon_url=ctx.author.avatar_url)
        embed.set_thumbnail(url=j[name]['picture'])
        await ctx.send(embed=embed)


    @commands.command()
    async def ugh(self, ctx):
        async with self.http.get('http://slipstream-bot.com/slipstream/weapons.json') as r:
            j = await r.json(content_type=None)
            await ctx.send(len(j))


    @commands.command()
    async def dm(self, ctx, user: discord.Member, *, message= 'aabbcc'):
        attachments = []

        for i in range(len(ctx.message.attachments)):
            attachments.append(ctx.message.attachments[i].url)
        attachments = "\n".join(attachments)

        if attachments == "":
            await user.send(f'{message}')

        elif message == 'aabbcc':
            await user.send(attachments)

        else:
            await user.send(f'{message} \n\nAttachments: {attachments}')

        await ctx.send('oi. i sent the DM. <a:hyperpartyblob:545832446890082314>')


    @commands.command()
    async def poll(self, ctx, options:int = 1):
        id = ctx.author.id
        channel = ctx.channel

        def check(ctx):
            return ctx.author.id == id and ctx.channel == channel

        if options == 1:
            msg = await ctx.send('Please input the poll.')
            poll = await self.bot.wait_for('message', check=check)
            picsqst = await ctx.send('Any Picture?')
            pics = await self.bot.wait_for('message', check=check)
            await picsqst.delete()
            await pics.delete()
            embed = discord.Embed(description=f'{poll.content}', color=embedColor.blurple)
            if pics.attachments != []:
                picture = await pics.attachents[0].to_file(use_cached=True)
                embed.set_image(url=pics.attachments[0].proxy_url)
                pollmsg = await ctx.send(picture=picture)
            else:
                pollmsg = await ctx.send('eh')
            await pollmsg.add_reaction('👍')
            await pollmsg.add_reaction('👎')
            await ctx.message.delete()
            await msg.delete()
            await poll.delete()
        if options > 1 and options < 9:
            optionlist = []
            reactionlist = ['🇦', '🇧', '🇨', '🇩', '🇪', '🇫', '🇬', '🇭', '🇮']
            for i in range(options):
                oh = await ctx.send(f'Please input the {i+1}. option.')
                msg = await self.bot.wait_for('message', check=check)
                optionlist.append(msg.content)
                await oh.delete()
                await msg.delete()
            embed = discord.Embed(title='New Poll Started', color=0x53dff)
            for i in range (options):
                embed.add_field(name=f'Option {reactionlist[i]}', value=f'{optionlist[i]}')
            embedmsg = await ctx.send(embed=embed)
            for i in range(options):
                await embedmsg.add_reaction(reactionlist[i])


    @commands.command()
    async def addmod(self, ctx):
        def is_correct(m):
            return m.author == ctx.author and m.channel == ctx.message.channel
        waitForList = ['Name =>', 'Ranks + Stats =>', 'URL =>']
        valueList = []
        for i in range(len(waitForList)):
            msg = await ctx.send(waitForList[i])
            a = await self.bot.wait_for('message', check=is_correct)
            valueList.append(a.content)
            await msg.delete()
            await a.delete()
        with open('mods.json', 'r') as j:
            j = str(j.read())
        f = open("mods.json","w+")
        f.write(j[:-1] + str(',"' + valueList[0] + '":{"name":"' + valueList[1] + '","ranksStats":"' +valueList[2]+ '","url":"'+valueList[3]+'"}}'))
        f.close()
        embed = discord.Embed(title=f'{valueList[0]} added successfully!', description=f"You've added:\nRanks and Stats: {valueList[1]}\nURL: {valueList[2]}", color=0x000072)
        await ctx.send(embed=embed)


    @commands.command()
    async def meh(self, ctx):
        async with self.http.get('http://slipstream-bot.com/slipstream/weapons.json') as r:
            print(r)


    @commands.command()
    async def uwu(self, ctx, *, message):
        message = message.replace('l', 'w')
        message = message.replace('r', 'w')
        await ctx.message.delete()
        await ctx.send(message)

        
    @commands.command()
    async def create(self, ctx):
        def is_correct(m):
            return m.author == ctx.author and m.channel == ctx.message.channel
        waitForList = ['Ight, name?', '1st ability?', 'description?', '2nd ability?', 'description?', '3rd ability?', 'description?', '4rd ability?', 'description?']
        valueList = []
        beep = True
        for i in range(len(waitForList)):
            msg = await ctx.send(waitForList[i])
            a = await self.bot.wait_for('message', check=is_correct)
            if a.content == 'Stop':
                await ctx.send('Stopped!')
                beep = False
                break
            else:
                valueList.append(a.content)
            await msg.delete()
            await a.delete()
        await ctx.send('o')
        if beep == True:
            embed = discord.Embed(title=valueList[0], description=valueList[1])
            embed.add_field(name=valueList[1], value=valueList[2], inline=False)
            embed.add_field(name=valueList[3], value=valueList[4], inline=False)
            embed.add_field(name=valueList[5], value=valueList[6], inline=False)
            embed.add_field(name=valueList[7], value=valueList[8], inline=False)
            await ctx.send(embed=embed)
            data = dict(name=valueList[0], desc=valueList[1], abil1=valueList[2], desc1=valueList[3])
            with open('yea.json', 'w') as file:
                json.dump(data, file)
            print(data)


    @commands.command()
    async def ouch(self, ctx):
        await ctx.message.delete()
        def is_correct(m):
            return m.author == ctx.author and m.channel == ctx.message.channel
        waitForList = ['testA', 'testB', 'testC']
        valueList = []
        for i in range(len(waitForList)):
            await ctx.send(waitForList[i])
            a = await self.bot.wait_for('message', check=is_correct)
            valueList.append(a.content)
        with open('hey.json', 'r') as file:
            yes = json.load(file)
        with open('hey.json', 'w+') as file:
            yes[valueList[2]]= dict(valA=valueList[0], valB=valueList[1])
            await ctx.send(yes)
            json.dump(yes, file)


    @commands.command()
    async def addweapon(self, ctx, modenumber: int =1):
        await ctx.message.delete()
        def is_correct(m):
            return m.author == ctx.author and m.channel == ctx.message.channel
        waitForList = ['Weapon Name =>', 'Firerate (eg. 12.5) =>', 'Mag Size =>', 'Reload Time (eg. 1.6s) =>', 'Total Damage =>', 'Crit Chance (eg. 12%) =>', 'Crit Multi (eg. 1.5x) =>', 'Status Chance (eg. 30%)=>', 'Slot (eg. Primary)=>', 'Mastery Requirement =>','Riven Disposition (eg. 1.45) =>', 'Trigger Type (eg. Auto) =>', 'Obtained =>', 'Weapon Range (eg. 2.5m) =>', 'Passive =>',  'Damage per Damage Type (eg. impact12.5, slash34.3) =>', 'Attack Speed (eg. 1.45)=>', 'Combo Duration (eg. 5s) =>', 'Type (eg. Heavy Blade) =>', 'Stance Polarity (eg. Madurai) =>', 'Innate Polarities (eg. Madurai, Varamon) =>', 'Picture link =>', 'Exilus Polarity (eg. Madurai, Varamon) =>']
        waitForList2 = ['Modename =>', 'Firerate (eg. 12.5) =>', 'Mag Size =>', 'Reload Time (eg. 1.6s) =>', 'Total Damage =>', 'Crit Chance (eg. 12%) =>', 'Crit Multi (eg. 1.5x) =>', 'Status Chance (eg. 30%)=>', 'Slot (eg. Primary)=>', 'Mastery Requirement =>','Riven Disposition (eg. 1.45) =>', 'Trigger Type (eg. Auto) =>', 'Obtained =>', 'Weapon Range (eg. 2.5m) =>', 'Passive =>',  'Damage per Damage Type (eg. impact12.5, slash34.3) =>', 'Attack Speed (eg. 1.45)=>', 'Combo Duration (eg. 5s) =>', 'Type (eg. Heavy Blade) =>', 'Stance Polarity (eg. Madurai) =>', 'Innate Polarities (eg. Madurai, Varamon) =>', 'Picture link =>', 'Exilus Polarity (eg. Madurai, Varamon) =>']
        valueList = [[]]
        beep = True
        if modenumber <= 0:
            await ctx.send('<:weird:718130997752758343>')
            return
            
        startmsg = await ctx.send('If a weapon does not have a stat, write "None"')

        for i in range(len(waitForList)):
            msg = await ctx.send(waitForList[i])
            a = await self.bot.wait_for('message', check=is_correct)
            if a.content.lower() == 'stop':
                await ctx.send('<:timetostop:722184630492463266>')
                beep = False
                break
            else:
                valueList[0].append(a.content)
            await msg.delete()
            await a.delete()

        if beep == True and modenumber > 1:
            for b in range(modenumber-1):
                valueList.append([])
                for i in range(len(waitForList2)):
                    msg = await ctx.send(waitForList2[i])
                    a = await self.bot.wait_for('message', check=is_correct)
                    if a.content.lower() == 'stop':
                        await ctx.send('<:timetostop:722184630492463266>')
                        beep = False
                        break
                    else:
                        valueList[b+1].append(a.content)
                    await msg.delete()
                    await a.delete()

        await startmsg.delete()

        def check(reaction, user):
            return reaction.message.id == wowamsmg.id and user == ctx.author

        embed = discord.Embed(title=f'You will add {valueList[0][0]} with these stats!', description=f"You've added:\nFire Rate: {valueList[0][1]}\nMag Size: {valueList[0][2]}\nReload Time: {valueList[0][3]}\nTotal Damage: {valueList[0][4]}\nCritical Chance: {valueList[0][5]}\nCritical Multiplier: {valueList[0][6]}\nStatus Chance: {valueList[0][7]}\nWeapon Slot: {valueList[0][8]}\nMastery Requirement: {valueList[0][9]}\nRiven Dispo: {valueList[0][10]}\nTriggertype: {valueList[0][11]}\nObtained: {valueList[0][12]}\nWeapon Range: {valueList[0][13]}\nPassive:{valueList[0][14]}\nDamage per Type: {valueList[0][15]}\nAttack Speed: {valueList[0][16]}\nCombo Duration: {valueList[0][17]}\nWeapon Type: {valueList[0][18]}\nStance Polarity: {valueList[0][19]}\nInnate Polarities: {valueList[0][20]}\nPicture Link: {valueList[0][21]}\nExilus Polarity: {valueList[0][22]}", color=0x000072)
        wowamsmg = await ctx.send(embed=embed)
        for i in range(1, len(valueList)):
            embed = discord.Embed(title=f'You will add the {valueList[i][0]} Mode with these stats!', description=f"You've added:\nFire Rate: {valueList[i][1]}\nMag Size: {valueList[i][2]}\nReload Time: {valueList[i][3]}\nTotal Damage: {valueList[i][4]}\nCritical Chance: {valueList[i][5]}\nCritical Multiplier: {valueList[i][6]}\nStatus Chance: {valueList[i][7]}\nWeapon Slot: {valueList[i][8]}\nMastery Requirement: {valueList[i][9]}\nRiven Dispo: {valueList[i][10]}\nTriggertype: {valueList[i][11]}\nObtained: {valueList[i][12]}\nWeapon Range: {valueList[i][13]}\nPassive:{valueList[i][14]}\nDamage per Type: {valueList[i][15]}\nAttack Speed: {valueList[i][16]}\nCombo Duration: {valueList[i][17]}\nWeapon Type: {valueList[i][18]}\nStance Polarity: {valueList[i][19]}\nInnate Polarities: {valueList[i][20]}\nPicture Link: {valueList[i][21]}\nExilus Polarity: {valueList[i][22]}", color=0x000072)
            await ctx.send(embed=embed)

        await wowamsmg.add_reaction('✅')
        await wowamsmg.add_reaction('❎')
        reaction, _ = await self.bot.wait_for('reaction_add', check=check)

        if reaction.emoji == '✅':
            with open('weapons.json', 'r') as file:
                yes = json.load(file)

            with open('weapons.json', 'w+') as file:
                yes[valueList[0][0]] = dict(uniqueName='None', fireRate=valueList[0][1], magazineSize=valueList[0][2], reloadTime=valueList[0][3], totalDamage=valueList[0][4], critchance=valueList[0][5], critMulti=valueList[0][6], statusChance=valueList[0][7], weaponSlot=valueList[0][8], masteryReq=valueList[0][9], rivenDispo=valueList[0][10], triggerType=valueList[0][11], obtained=valueList[0][12], range=valueList[0][13], passive=valueList[0][14], damagewTypes=valueList[0][15], attackSpeed=valueList[0][16], comboDur=valueList[0][17], weaponType=valueList[0][18], stancePol=valueList[0][19], inPol=valueList[0][20], picture=valueList[0][21], exPol=valueList[0][22])
                for i in range(1, len(valueList)):
                    yes[valueList[0][0]][f'Mode{i+1}'] = dict(modeName=valueList[i][0], magazineSize=valueList[i][2], reloadTime=valueList[i][3], totalDamage=valueList[i][4], critchance=valueList[i][5], critMulti=valueList[i][6], statusChance=valueList[i][7], weaponSlot=valueList[i][8], masteryReq=valueList[i][9], rivenDispo=valueList[i][10], triggerType=valueList[i][11], obtained=valueList[i][12], range=valueList[i][13], passive=valueList[i][14], damagewTypes=valueList[i][15], attackSpeed=valueList[i][16], comboDur=valueList[i][17], weaponType=valueList[i][18], stancePol=valueList[i][19], inPol=valueList[i][20], picture=valueList[i][21], exPol=valueList[i][22])
                json.dump(yes, file)
            nye = await ctx.send(f'{valueList[0][0]} added successfully!')
            okty = await ctx.send('<a:kannapoggies:715166570703487038>')
            await nye.delete(delay=3)
            await okty.delete(delay=3)

        if reaction.emoji == '❎':
            await ctx.send('Weapon not added!')
        await wowamsmg.delete()


    @commands.command(aliases=['pricecheck'])
    @commands.bot_has_permissions(embed_links=True)
    async def pc(self, ctx, *, item):
        pcitem = item.lower()
        pcitem = item.replace(' ', '_')

        try:
            async with self.http.get(f'https://api.warframe.market/v1/items/{pcitem}/statistics') as r:
                j = await r.json(content_type=None)
            avgprice = 0

            for i in range(len(j['payload']['statistics_closed']['48hours'])):
                avgprice += j['payload']['statistics_closed']['48hours'][i]['closed_price']
            avgprice = avgprice/len(j['payload']['statistics_closed']['48hours'])
            
            embed = discord.Embed(title=f'Price check for {item.title()}', description=f'Average Price: {round(avgprice, 2)} <:platinum:565728007206076417>', color=embedColor.blurple)
            embed.timestamp = datetime.utcnow()
            embed.set_footer(text=f'{ctx.author.name}#{ctx.author.discriminator}', icon_url=ctx.author.avatar_url)
            await ctx.send(embed=embed)

        except ZeroDivisionError:
            def check(reaction, user):
                return reaction.message.id == msg.id and user == ctx.author

            embed = discord.Embed(description='Not enough recent trade data to fetch prices! Would you like to see the data for the last 90 days?\n Keep in mind that this data might not be accurate.', color=embedColor.orange)
            embed.timestamp = datetime.utcnow()
            embed.set_footer(text=f'{ctx.author.name}#{ctx.author.discriminator}', icon_url=ctx.author.avatar_url)
            msg = await ctx.send(embed=embed)
            await msg.add_reaction('✅')
            await msg.add_reaction('❎')
            reaction, _ = await self.bot.wait_for('reaction_add', check=check)

            if reaction.emoji == '✅':
                try:
                    avgprice = 0
                    for i in range(len(j['payload']['statistics_closed']['90days'])):
                        avgprice += j['payload']['statistics_closed']['90days'][i]['closed_price']
                    avgprice = avgprice/len(j['payload']['statistics_closed']['90days'])
            
                    embed = discord.Embed(title=f'Price check for {item.title()}', description=f'Average Price: {round(avgprice, 2)} <:platinum:565728007206076417>', color=embedColor.blurple)
                    embed.timestamp = datetime.utcnow()
                    embed.set_footer(text=f'{ctx.author.name}#{ctx.author.discriminator}', icon_url=ctx.author.avatar_url)
                except ZeroDivisionError:
                    embed = discord.Embed(description='Not enough trade data to fetch prices!', color=embedColor.orange)
                    embed.timestamp = datetime.utcnow()
                    embed.set_footer(text=f'{ctx.author.name}#{ctx.author.discriminator}', icon_url=ctx.author.avatar_url)
                await msg.edit(embed=embed)
            if reaction.emoji == '❎':
                pass

        except KeyError:
            embed = discord.Embed(description='Something went wrong!\n> Make sure that the spelling is correct.\n> Make sure the item is tradeable.\n> If it\'s a prime set, put set afterwards.\n> Make sure it\'s worth more than 1 plat.', color=embedColor.orange)
            embed.timestamp = datetime.utcnow()
            embed.set_footer(text=f'{ctx.author.name}#{ctx.author.discriminator}', icon_url=ctx.author.avatar_url)
            await ctx.send(embed=embed)


    @commands.command()
    async def relic(self, ctx, era, name, state):
        era = era.title()
        name = name.upper()
        state = state.title()
        with open('relicconts.json', 'r') as file:
            relicdata = json.load(file)
        for i in range(len(relicdata['relics'])):
            if relicdata['relics'][i]['tier'] == era and relicdata['relics'][i]['relicName'] == name and relicdata['relics'][i]['state'] == state:
                await ctx.send(relicdata['relics'][i]['rewards'])


    @commands.command()
    async def whereis(self, ctx, *, item):
        item = item.title()
        with open('relicconts.json', 'r') as file:
            relicdata = json.load(file)
        whereisl = []
        for i in range(len(relicdata['relics'])):
            for g in range(6):
                if relicdata['relics'][i]['rewards'][g]['itemName'] == item and f"{relicdata['relics'][i]['tier']} {relicdata['relics'][i]['relicName']}" not in whereisl:
                    whereisl.append(f"{relicdata['relics'][i]['tier']} {relicdata['relics'][i]['relicName']}")
        if whereisl != []:
            embed = discord.Embed(title=f'Locations of {item}', description='\n'.join(whereisl), color=embedColor.blurple)
        else:
            embed = discord.Embed(description=f'Locations of {item} could not be found. please Check the spelling and if its in a relic or not. If it isnt, use ', color=embedColor.red)

        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(maincommands(bot))
