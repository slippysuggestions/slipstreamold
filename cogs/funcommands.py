import discord
import random
import urllib
import json
import aiohttp
import time
import asyncio
import re
from discord.ext import commands, tasks
from discord import utils
from random import sample, choice
from datetime import datetime


bot = commands.Bot(command_prefix='!',
                   description='no')


class funcommands(commands.Cog):
    def __init__(self, bot):
        self.bot:commands.Bot = bot
        self._last_result = None


    @commands.command()
    async def passgen(self, ctx, arg):
        passwordchars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~`!@#$%^&*()_;-+={[}:]|<,>.?'
        if int(arg) >= len(passwordchars):
            await ctx.send('Please choose a valid length. Length cannot be negative or larger than 90.')
        if int(arg) < 0:
            await ctx.send('Please choose a valid length. Length cannot be negative or larger than 90.')
        else:
            newpass = ''.join(random.sample(passwordchars, int(arg)))
            await ctx.send(newpass)


    @commands.command()
    async def randomloadout(self, ctx):
        warframes = ['Ash', 'Atlas', 'Banshee', 'Baruuk', 'Chroma', 'Ember', 'Equinox', 'Excalibur', 'Frost', 'Gara',
                     'Garuda',
                     'Gauss', 'Grendel', 'Harrow', 'Hildryn', 'Hydroid', 'Inaros', 'Ivara', 'Khora', 'Limbo', 'Loki',
                     'Mag',
                     'Mesa', 'Mirage', 'Nekros', 'Nezha', 'Nidus', 'Nova', 'Nyx', 'Oberon', 'Octavia', 'Revenant',
                     'Rhino',
                     'Saryn', 'Titania', 'Trinity', 'Valkyr', 'Vauban', 'Volt', 'Wisp', 'Wukong', 'Zephyr', 'Protea']
        primaries = ['Braton', 'MK1-Braton', 'Burston', 'Hind', 'Latron', 'MK1-Paris', 'Snipetron', 'MK1-Strun', 'Karak',
                     'Strun', 'Boar', 'Boltor', 'Mutalist Quanta', 'Vectis', 'Gorgon', 'Paris', 'Tetra', 'Vulkar', 'Dera',
                     'Hek', 'Quanta', 'Prisma Tetra', 'Torid', 'Drakgoon', 'Dread', 'Grakata', 'Ignis', 'Kohm', 'Simulor',
                     'Snipetron Vandal', 'Sybaris', 'Tonkor', 'Cernos', 'Flux Rifle', 'Miter', 'Penta', 'Rubico', 'Scourge',
                     'Soma', 'Argonak', 'Attica', 'Baza', 'Convectrix', 'Dera Vandal', 'Dex Sybaris', 'Gorgon Wraith',
                     'Grinlok', 'Harpak', 'Hema', 'Javlok', 'Karak Wraith', 'Latron Wraith', 'Mutalist Cernos', 'Panthera',
                     'Paracyst', 'Sobek', 'Soma Prime', 'Tigris', 'Vulkar Wraith', 'Zarr', 'Acceltra', 'Braton Prime',
                     'Corinth', 'Exergis', 'Fulmin', 'Glaxion', 'Komorex', 'Lenz', 'Paris Prime', 'Stradavar', 'Veldt',
                     'Ignis Wraith', 'Nagantaka', 'Ogris', 'Phantasma', 'Amprex', 'Arca Plasmor', 'Astilla', 'Battacor',
                     'Baza Prime', 'Daikyu', 'Lanka', 'Latron Prime', 'Quanta Vandal', 'Quartakk', 'Strun Wraith', 'Tenora',
                     'Tiberon', 'Zenith', 'Zhuge', 'Boar Prime', 'Buzlok', 'Prisma Gorgon', 'Prisma Grakata',
                     'Prisma Grinlok', 'Phage', 'Synapse', 'Telos Boltor', 'Burston Prime', 'Cernos Prime', 'Rakta Cernos',
                     'Glaxion Vandal', 'Vaykor Hek', 'Secura Penta', 'Quellor', 'Rubico Prime', 'Synoid Simulor',
                     'Stradavar Prime', 'Supra', 'Sybaris Prime', 'Sancti Tigris', 'Boltor Prime', 'Kuva Drakgoon',
                     'Kuva Karak', 'Kuva Kohm', 'Kuva Quartakk', 'Kuva Tonkor', 'Shedu', 'Tigris Prime', 'Ferrox',
                     'Opticor', 'Opticor Vandal', 'Supra Vandal', 'Tiberon Prime', 'Vectis Prime', 'Zhuge Prime']
        secondaries = ['Bronco', 'MK1-Furis', 'Kraken', 'MK1-Kunai', 'Lato', 'Seer', 'A Kitgun	', 'Akbronco',
                       'Ballistica', 'Furis', 'Gammacor', 'Hikou', 'Kunai', 'Sonicor', 'Stug', 'Aklato', 'Castanas', 'Lex',
                       'Sicarus', 'Afuris', 'Aklex', 'Akzani', 'Angstrum', 'Bronco', 'Prime', 'Cestra', 'Despair',
                       'Hikou Prime', 'Nukor', 'Spectra', 'Vasto', 'Viper', 'Viper Wraith', 'Atomos', 'Kohmak', 'Kulstar',
                       'Twin Gremlins', 'Twin Vipers', 'Azima', 'Rakta Ballistica', 'Brakk', 'Detron', 'Plinx', 'Zylok',
                       'Acrid', 'Bolto', 'Dual Cestra', 'Fusilai', 'Synoid Gammacor', 'Hystrix', 'Lato Vandal', 'Marelok',
                       'Stubba', 'Twin Vipers Wraith', 'Akarius', 'Akjagara', 'Akstiletto', 'Akvasto', 'Prisma Angstrum',
                       'Cyanex', 'Cycron', 'Lex Prime', 'Ocucor', 'Pandero', 'Spira', 'Talons', 'Akbolto', 'Aksomati',
                       'Mara Detron', 'Embolist', 'Pox', 'Quatz', 'Twin Grakatas', 'Twin Rogga', 'Tysis', 'Akbronco Prime',
                       'Akstiletto Prime', 'Arca Scisco', 'Sancti Castanas', 'Dex Furis', 'Secura Dual Cestra', 'Knell',
                       'Magnus', 'Vaykor Marelok', 'Spectra Vandal', 'Spira Prime', 'Staticor', 'Twin Kohmak',
                       'Vasto Prime', 'Zakti', 'Telos Akbolto', 'Dual Toxocyst', 'Prisma Twin Gremlins', 'Akjagara Prime',
                       'Akmagnus', 'Aksomati Prime', 'Akvasto Prime', 'Pyrana', 'Akbolto Prime', 'Kuva Brakk', 'Kuva Nukor',
                       'Kuva Twin Stubbas', 'Pyrana Prime', 'Ballistica Prime', 'Euphona Prime', 'Sicarus Prime',
                       'Aklex Prime', 'Kuva Kraken', 'Kuva Seer']
        melees = ['MK1-Bo', 'Cronus', 'Dual Skana', 'Fang', 'MK1-Furax', 'Jaw Sword', 'Kestrel', 'Lecta', 'Silva & Aegis', 
                  'Skana', 'A Zaw', 'Dual Kamas', 'Glaive', 'Kama', 'Machete', 'Magistar', 'Ankyros', 'Dark Dagger',
                  'Dual Zoren', 'Fragor', 'Gram', 'Kogake', 'Nami Skyla', 'Orthos', 'Scindo', 'Ack & Brunt', 'Anku',
                  'Ceramic Dagger', 'Dual Heat Swords', 'Galatine', 'Heat Dagger', 'Heat Sword', 'Kronen', 'Pangolin Sword',
                  'Prova', 'Tonbo', 'Bo', 'Boltace', 'Ether Reaper', 'Guandao', 'Nikana', 'Obex', 'Plasma Sword',
                  'Redeemer', 'Venka', 'Amphis', 'Atterax', 'Cassowar', 'Dark Split-Sword', 'Dual Cleavers', 'Furax',
                  'Gazal Machete', 'Jat Kittag', 'Mire', 'Okina', 'Orvius', 'Ripkas', 'Sheev', 'Sydon', 'Dex Dakra',
                  'Dual Ichor', 'Dual Raza', 'Ether Daggers', 'Galvacord', 'Karyst', 'Kreska', 'Nami Solo', 'Scoliac',
                  'Serro', 'Sibear', 'Tekko', 'Tipedo', 'Zenistar', 'Broken Scepter', 'Caustacyst', 'Cerata', 'Destreza',
                  'Dual Keres', 'Endura', 'Ether Sword', 'Halikar', 'Hirudo', 'Kesheg', 'Lacera', 'Pennant', 'Pupacyst',
                  'Tatsu', 'Twin Basolk', 'Wolf Sledge', 'Ankyros Prime', 'Rakta Dark Dagger', 'Dark Sword',
                  'Dragon Nikana', 'Dual Ether', 'Dual Kamas Prime', 'Falcor', 'Gunsen', 'Hate', 'Secura Lecta',
                  'Sancti Magistar', 'Masseter', 'Mios', 'Ninkondi', 'Ohma', 'Prova Vandal', 'Sarpa', 'Scindo Prime',
                  'Prisma Skana', 'Prisma Dual Cleavers', 'Furax Wraith', 'Heliocor', 'Korrudo', 'Krohkur', 'Lesion',
                  'Pathocyst', 'Volnus', 'Arca Titron', 'Bo Prime', 'Broken War', 'Cobra & Crane', 'Dakra Prime',
                  'Destreza Prime', 'Fang Prime', 'Glaive Prime', 'Kogake Prime', 'Prisma Obex', 'Paracesis', 
                  'Reaper Prime', 'Redeemer Prime', 'Shaku', 'Sigma & Octantis', 'Tipedo Prime', 'Twin Krohkur', 'War', 
                  'Telos Boltace', 'Synoid Heliocor', 'Jat Kusar', 'Machete Wraith', 'Nami Skyla Prime', 'Skiajati', 
                  'Vaykor Sydon', 'Fragor Prime', 'Nikana Prime', 'Orthos Prime', 'Silva & Aegis Prime', 'TekkoPrime', 
                  'Galatine Prime', 'Kronen Prime', 'Kuva Shildeg', 'Gram Prime', 'Ninkondi Prime', 'Venka Prime', ] 
        loadout = ''.join(random.sample(warframes, 1)) + ', ' + ''.join(random.sample(primaries, 1)) + ', ' + ''.join(
            random.sample(secondaries, 1)) + ', ' + ''.join(random.sample(melees, 1))
        await ctx.send(loadout)


    @commands.command()
    async def ehp(self, ctx, health: int, armor: int):
        newarmor = armor + 300
        newarmor = newarmor / 300
        newarmor = newarmor * health
        embed = discord.Embed(title='Effective Hitpoints',
                              color=0x52fffc,
                              description= f'Effective Hitpoints with {health} Health and {armor} Armor: {str(newarmor)[0:7]}')
        embed.set_footer(text= 'Health first, Armor Second')
        await ctx.send(embed=embed)


    @commands.command()
    async def dr(self, ctx, armor: int):
        newarmor = float(armor) + 300
        newarmor = armor / newarmor
        newarmor = newarmor * 100
        embed = discord.Embed(title='Damage Reduction',
                              color=0x52fffc,
                              description=f'Damage Reduction with {armor} Armor: {str(newarmor)[0:5]}%')
        await ctx.send(embed=embed)


    @commands.command()
    async def snake(self, ctx):
        await ctx.message.delete()
        points = 0
        pointStatus = "Reached"
        yVal = [0]
        xVal = [0]
        field = [['<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>'],['<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>'],['<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>'],['<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>'],['<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bg:699550446515388456>','<:bonk:699551539127386122>']]
        embed = discord.Embed(title=f'Points: {points}', description=str(''.join(field[0])) + '\n' + str(''.join(field[1])) + '\n' +  str(''.join(field[2])) + '\n' +  str(''.join(field[3])) + '\n' +  str(''.join(field[4])))
        msg = await ctx.send(embed=embed)
        await msg.add_reaction('⬅️')
        await msg.add_reaction('⬆️')
        await msg.add_reaction('➡️')
        await msg.add_reaction('⬇️')
        await msg.add_reaction('❌')
        playerpos = [4,9]
        oof = "Yes"
        pointspawner = 0

        def segments():
            for i in range(len(playerpos)-1, 2, -2):
                field[playerpos[i-1]][playerpos[i]] = '<:bg:699550446515388456>'

            for i in range(len(playerpos)-1, 2, -2):
                playerpos[i] = playerpos[i-2]
                playerpos[i-1] = playerpos[i-3]
                
            for i in range(len(playerpos)-1, 2, -2):
                field[playerpos[i-1]][playerpos[i]] = '<:bonk:699551539127386122>'
                
        def check(reaction, user):
            return reaction.message.id == msg.id and user == ctx.author
        while oof == "Yes":
            try:
                reaction, _ = await self.bot.wait_for('reaction_add', check=check, timeout=60.0)

                if reaction.emoji == '⬅️' and playerpos[1] > 0:
                    field[playerpos[0]][playerpos[1]] = '<:bg:699550446515388456>'
                    playerpos[1] = playerpos[1] - 1
                    field[playerpos[0]][playerpos[1]] = '<:bonk:699551539127386122>'
                    if len(playerpos) > 2:
                        segments()

                elif reaction.emoji == '➡️' and playerpos[1] < 10:
                    field[playerpos[0]][playerpos[1]] = '<:bg:699550446515388456>'
                    playerpos[1] = playerpos[1] + 1
                    field[playerpos[0]][playerpos[1]] = '<:bonk:699551539127386122>'
                    if len(playerpos) > 2:
                        segments()

                elif reaction.emoji == '⬇️' and playerpos[0] < 5:
                    field[playerpos[0]][playerpos[1]] = '<:bg:699550446515388456>'
                    playerpos[0] = playerpos[0] + 1
                    field[playerpos[0]][playerpos[1]] = '<:bonk:699551539127386122>'
                    if len(playerpos) > 2:
                        segments()

                elif reaction.emoji == '⬆️' and playerpos[0] > 0:
                    field[playerpos[0]][playerpos[1]] = '<:bg:699550446515388456>'
                    playerpos[0] = playerpos[0] - 1
                    field[playerpos[0]][playerpos[1]] = '<:bonk:699551539127386122>'
                    if len(playerpos) > 2:
                        segments()

                pointspawner += 1
                if pointspawner >= 5 and pointStatus == "Reached":
                    while True:
                        yVal = random.sample([0,1,2,3,4,5,6,7,8,9], 1)
                        xVal = random.sample([0,1,2,3,4], 1)

                    pointspawner = 0
                    pointStatus = "Not Reached"
                    field[xVal[0]][yVal[0]] = '<:point:699553198783987752>'
                if playerpos[0] == xVal[0] and playerpos[1] == yVal[0]:
                    points += 1
                    pointStatus = "Reached"
                    playerpos.append(xVal[0])
                    playerpos.append(yVal[0])
                embed = discord.Embed(title=f'Points: {points}', description=f'{"".join(field[0])}\n{"".join(field[1])}\n{"".join(field[2])}\n{"".join(field[3])}\n{"".join(field[4])}')

                if reaction.emoji == '❌':
                    oof = "No"
                    embed = discord.Embed(title=f'You reached {points} Points!')

                await msg.edit(embed=embed)
                await msg.remove_reaction(reaction.emoji, ctx.author)

            except asyncio.TimeoutError:
                    oof = "No"
                    embed = discord.Embed(title=f'You reached {points} Points!')
                    await msg.edit(embed=embed)

        await msg.clear_reactions()


    @commands.command(aliases=['vlf', 'fusion'])
    async def valencefusion(self, ctx, *, weampoms):
        weampoms = weampoms.lower()
        list = weampoms.split('-')
        percentages = []
        try:
            for i in range(len(list)):
                percent = re.findall('[2-6][0-9]', list[i])
                if int(percent[0]) > 24 and int(percent[0]) < 61:
                    percentages.append(int(percent[0]))
            if percentages[0] > percentages[1]:
                percentages = percentages[0]*1.1
            else:
                percentages = percentages[1]*1.1
            if percentages > 60:
                percentages = 60
            await ctx.send(f'New Kuva Weapon has {str(percentages)[0:5]}% Elemental Damage!')
        except IndexError:
            await ctx.send('Not valid! Try again.')
            return

def setup(bot):
    bot.add_cog(funcommands(bot))
