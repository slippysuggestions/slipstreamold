import discord
import random
import json
import aiohttp
import time
import asyncio
import inspect
from discord.ext import commands, tasks
from discord import utils
from datetime import datetime, timedelta

bot = commands.Bot(command_prefix='!',
                   description='no')
bot.remove_command('help')
initial_extensions = ['funcommands', 'maincommands', 'admin']

session = aiohttp.ClientSession()

class embedColor:
    orange = 0xec5c17
    yellow = 0xFFFF00
    blurple = 0x7289DA
    blue = 0x499FFF 
    darkblue = 0x000072
    grey = 0xCCCCCC 
    red = 0xc90b0b
    green = 0x00ff0f


@bot.event
async def on_message(message):
    if message.guild is None and message.author != bot.user:
        channel = bot.get_channel(696274975690063903)

        if 'discord.gg/' in message.content:
            await channel.send('<@206212600596398080>, someone is advertising!')
            embed = discord.Embed(description='``` ' + message.content + f'```\nUser: {message.author}\nUser ID: {message.author.id}',
                                  color=embedColor.orange)
        else:
            attachments = []
            for i in range(len(message.attachments)):
                attachments.append(message.attachments[i].url)
            attachments = "\n".join(attachments)
            if message.content != "":
                message.content = '```' + message.content + '```'
            if attachments == "":
                embed = discord.Embed(description=message.content + f'\nUser: {message.author}\nUser ID: {message.author.id}')
            else:
                embed = discord.Embed(description=message.content + f'\nAttachments: {attachments} \nUser: {message.author}\nUser ID: {message.author.id}')

        embed.set_author(name='Dm Received', icon_url=message.author.avatar_url)
        embed.set_footer(text=f'{str(datetime.utcnow()).split(".")[0]}')

        await channel.send(embed=embed)

    await bot.process_commands(message)


@bot.event
async def on_ready():
    # status_timer.start()
    await bot.change_presence(activity=discord.Activity(name=f"boo", type=discord.ActivityType.playing))


@bot.command(aliases= ["shut", "sd"])
async def shutdown(ctx):
    await session.close()
    embed = discord.Embed(description=':robot_face: Shutting Down Now', color=0x52fffc,)
    await ctx.send(embed=embed)
    await ctx.bot.close()
    print(f'{bot.user} shutting down')


# @tasks.loop(seconds=60)
# async def status_timer():
#     async with session.get('http://content.warframe.com/dynamic/worldState.php') as r:
#         anom = await r.json(content_type = None)

#     async with session.get('https://ws.warframestat.us/pc/cetusCycle') as h:
#         cetus = await h.json(content_type= None)

#     anomalypos = anom['Tmp']
#     expiry = datetime.strptime(cetus["expiry"][:-5].replace("T", "-"), "%Y-%m-%d-%H:%M:%S")
#     timeleft = expiry - datetime.utcnow()
#     cetusState = 'No'

#     if cetus['isDay'] == True:
#         cetusState = '🌞'
#     if cetus['isDay'] == False:
#         cetusState = '🌙'

#     def anomalyposition(thing):
#         return {
#             '[]':'Not active',
#             '{"sfn":505}':'Ruse War Field',
#             '{"sfn":510}':'Gian Point',
#             '{"sfn":550}':'Nsu Grid',
#             '{"sfn":551}':'Ganalen`s Grave',
#             '{"sfn":552}':'Rya',
#             '{"sfn":553}':'Flexa',
#             '{"sfn":554}':'H-2 Cloud',
#             '{"sfn":555}':'R-9 Cloud'
#         }.get(thing, 'Not Yet Defined')

#     await bot.change_presence(activity=discord.Activity(name=f"{cetusState}: {str(timeleft)[:-7]} left • Anomaly: {anomalyposition(anomalypos)}", type=discord.ActivityType.playing))
#     await asyncio.sleep(30)

#     timeleft = expiry - datetime.utcnow()
#     await bot.change_presence(activity=discord.Activity(name=f"{cetusState}: {str(timeleft)[:-7]} left • Anomaly: {anomalyposition(anomalypos)}", type=discord.ActivityType.playing))
 

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.send('Check Failed you dummy')
    if isinstance(error, commands.errors.CommandNotFound):
        pass
    else:
        await ctx.send(f'Error! `{error}`')


# async def fortuna_cycles():
#     async with session.get('https://ws.warframestat.us/pc/vallisCycle') as r:
#         j = await r.json(content_type=None)
#     channel = bot.get_channel(677117997957251106)
#     await channel.send(j)
#     status = j['isWarm']
#     while True:
#         await asyncio.sleep(60)
#         async with session.get('https://ws.warframestat.us/pc/vallisCycle') as r:
#             j = await r.json(content_type=None)
#         if status == j['isWarm']:
#             pass
#         else:
#             status = j['isWarm']
#             await channel.send(f'Is it warm? {j["isWarm"]}')


# @bot.command()
# async def eval_(ctx, *, command):
#     res = eval(command)
#     if inspect.isawaitable(res):
#         await res
#     else:
#         res



if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            bot.load_extension(f'cogs.{extension}')
        except Exception as e:
            print(e)
    bot.run('TOKEN')
